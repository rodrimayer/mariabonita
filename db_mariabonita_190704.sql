-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 04-07-2019 a las 18:15:16
-- Versión del servidor: 5.5.24-log
-- Versión de PHP: 5.4.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `db_mariabonita`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `articulo`
--

CREATE TABLE IF NOT EXISTS `articulo` (
  `codigo` varchar(8) NOT NULL,
  `id_tipo` int(11) DEFAULT NULL,
  `id_talle` int(11) DEFAULT NULL,
  `precio_costo` float DEFAULT NULL,
  `precio_venta` float DEFAULT NULL,
  `stock` int(11) DEFAULT NULL,
  `desc_articulo` varchar(50) NOT NULL,
  PRIMARY KEY (`codigo`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `articulo`
--

INSERT INTO `articulo` (`codigo`, `id_tipo`, `id_talle`, `precio_costo`, `precio_venta`, `stock`, `desc_articulo`) VALUES
('RS-345', 17, 2, 1400, 2800, 1, 'Vestido de Fiesta Negro'),
('RX-22', 14, 9, 150, 250, 1, 'Remera cuello en V Blanca'),
('RI5', 14, 6, 300, 750, 1, 'Remera Color Crema'),
('VU30', 17, 2, 850, 1900, 1, 'Vestido Corto Verde Marca Las Locas'),
('XR242', 14, 1, 120, 400, 1, 'Remera básica negra MariaBonita'),
('CPCC', NULL, NULL, NULL, NULL, NULL, 'Cancelación Parcial Cta Cte'),
('CCCC1', NULL, NULL, NULL, NULL, NULL, 'Cancelación Cuota 1 Cuenta Corriente'),
('CTCC', NULL, NULL, NULL, NULL, NULL, 'Cancelación Total Cuenta Corriente'),
('CCCC2', NULL, NULL, NULL, NULL, NULL, 'Cancelación Cuota 2 Cuenta Corriente'),
('CCCC3', NULL, NULL, NULL, NULL, NULL, 'Cancelación Cuota 3 Cuenta Corriente'),
('CCCC4', NULL, NULL, NULL, NULL, NULL, 'Cancelación Cuota 4 Cuenta Corriente'),
('CCCC5', NULL, NULL, NULL, NULL, NULL, 'Cancelación Cuota 5 Cuenta Corriente'),
('CCCC6', NULL, NULL, NULL, NULL, NULL, 'Cancelación Cuota 6 Cuenta Corriente'),
('CCCC7', NULL, NULL, NULL, NULL, NULL, 'Cancelación Cuota 7 Cuenta Corriente'),
('CCCC8', NULL, NULL, NULL, NULL, NULL, 'Cancelación Cuota 8 Cuenta Corriente'),
('CCCC9', NULL, NULL, NULL, NULL, NULL, 'Cancelación Cuota 9 Cuenta Corriente'),
('CCCC10', NULL, NULL, NULL, NULL, NULL, 'Cancelación Cuota 10 Cuenta Corriente'),
('CCCC11', NULL, NULL, NULL, NULL, NULL, 'Cancelación Cuota 11 Cuenta Corriente'),
('CCCC12', NULL, NULL, NULL, NULL, NULL, 'Cancelación Cuota 12 Cuenta Corriente');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `boleta`
--

CREATE TABLE IF NOT EXISTS `boleta` (
  `id_boleta` int(11) NOT NULL AUTO_INCREMENT,
  `fecha_boleta` date NOT NULL,
  `id_cliente` int(11) NOT NULL,
  `total` float NOT NULL,
  `cerrada` tinyint(1) NOT NULL,
  `vendedor` int(11) NOT NULL,
  PRIMARY KEY (`id_boleta`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=44 ;

--
-- Volcado de datos para la tabla `boleta`
--

INSERT INTO `boleta` (`id_boleta`, `fecha_boleta`, `id_cliente`, `total`, `cerrada`, `vendedor`) VALUES
(43, '2019-06-26', 12321455, 1330, 0, 1),
(42, '2019-06-26', 13609436, 3875, 0, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `caja_diaria`
--

CREATE TABLE IF NOT EXISTS `caja_diaria` (
  `id_caja` int(11) NOT NULL AUTO_INCREMENT,
  `fecha_caja` date NOT NULL,
  PRIMARY KEY (`id_caja`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliente`
--

CREATE TABLE IF NOT EXISTS `cliente` (
  `id_cliente` int(11) NOT NULL,
  `nombre_cliente` varchar(20) NOT NULL,
  `apellido_cliente` varchar(20) NOT NULL,
  `direccion` varchar(30) NOT NULL,
  `localidad` varchar(20) NOT NULL,
  `telefono` varchar(20) NOT NULL,
  `mail` varchar(30) NOT NULL,
  PRIMARY KEY (`id_cliente`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `cliente`
--

INSERT INTO `cliente` (`id_cliente`, `nombre_cliente`, `apellido_cliente`, `direccion`, `localidad`, `telefono`, `mail`) VALUES
(37305672, 'Rodri', 'Mayer', 'Maipu', 'LGSM', '3886525523', 'rodrimmayer@gmail.com'),
(1234555, 'Martin', 'Perez', 'Juan XXIII', 'LGSM', '123213123', 'adssadad'),
(12321455, 'Maria Agostina', 'Rubio', 'Barrio Santa Rosa', 'LGSM', '12312552', 'sadsadsad'),
(11111111, 'Consumidor', 'Final', '-', '-', '111111111', ''),
(13609436, 'Reynaldo', 'Mayer', 'Maipu', 'LGSM', '3886525487', 'rmayer@ledesma.com.ar');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `color`
--

CREATE TABLE IF NOT EXISTS `color` (
  `id_color` int(11) NOT NULL,
  `desc_color` varchar(20) NOT NULL,
  PRIMARY KEY (`id_color`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cond_vta`
--

CREATE TABLE IF NOT EXISTS `cond_vta` (
  `id_condvta` int(11) NOT NULL AUTO_INCREMENT,
  `desc_condvta` varchar(20) NOT NULL,
  `permite_cuotas` tinyint(1) NOT NULL,
  PRIMARY KEY (`id_condvta`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Volcado de datos para la tabla `cond_vta`
--

INSERT INTO `cond_vta` (`id_condvta`, `desc_condvta`, `permite_cuotas`) VALUES
(1, 'VISA DEBITO', 0),
(2, 'VISA CREDITO', 1),
(3, 'MASTERCARD DEBITO', 0),
(4, 'MASTERCARD CREDITO', 1),
(5, 'NARANJA DEBITO', 0),
(6, 'NARANJA CREDITO', 1),
(7, 'MERCADOPAGO', 0),
(8, 'CONTADO', 0),
(9, 'CUENTA CORRIENTE', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `configuracion`
--

CREATE TABLE IF NOT EXISTS `configuracion` (
  `id_config` int(11) NOT NULL AUTO_INCREMENT,
  `desc_config` varchar(30) NOT NULL,
  `valor` int(11) NOT NULL,
  PRIMARY KEY (`id_config`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `configuracion`
--

INSERT INTO `configuracion` (`id_config`, `desc_config`, `valor`) VALUES
(1, 'dia_vto_cuotas', 15),
(2, 'limite_deuda', 5000);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cta_cte`
--

CREATE TABLE IF NOT EXISTS `cta_cte` (
  `id_ctacte` int(11) NOT NULL AUTO_INCREMENT,
  `id_cliente` int(11) NOT NULL,
  PRIMARY KEY (`id_ctacte`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Volcado de datos para la tabla `cta_cte`
--

INSERT INTO `cta_cte` (`id_ctacte`, `id_cliente`) VALUES
(1, 37305672),
(2, 13609436),
(3, 12321455);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle_boleta`
--

CREATE TABLE IF NOT EXISTS `detalle_boleta` (
  `id_detalleboleta` int(11) NOT NULL AUTO_INCREMENT,
  `id_boleta` int(11) NOT NULL,
  `codigo` varchar(20) NOT NULL,
  `descuento` int(11) NOT NULL,
  `precio_total` float NOT NULL,
  PRIMARY KEY (`id_detalleboleta`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=59 ;

--
-- Volcado de datos para la tabla `detalle_boleta`
--

INSERT INTO `detalle_boleta` (`id_detalleboleta`, `id_boleta`, `codigo`, `descuento`, `precio_total`) VALUES
(58, 43, 'VU30', 30, 1330),
(57, 42, 'RS-345', 0, 2800),
(56, 42, 'XR242', 0, 400),
(55, 42, 'RI5', 10, 675);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle_caja`
--

CREATE TABLE IF NOT EXISTS `detalle_caja` (
  `id_detallecaja` int(11) NOT NULL AUTO_INCREMENT,
  `id_caja` int(11) NOT NULL,
  `id_condvta` int(11) NOT NULL,
  `cant_cuotas` int(11) NOT NULL,
  `total` float NOT NULL,
  PRIMARY KEY (`id_detallecaja`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle_ctacte`
--

CREATE TABLE IF NOT EXISTS `detalle_ctacte` (
  `id_detallectacte` int(11) NOT NULL AUTO_INCREMENT,
  `id_ctacte` int(11) NOT NULL,
  `numero_cuota` int(11) NOT NULL,
  `vencimiento_cuota` date NOT NULL,
  `valor_cuota` float NOT NULL,
  `valor_cancelado` float NOT NULL,
  `finalizado` tinyint(1) NOT NULL,
  PRIMARY KEY (`id_detallectacte`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=70 ;

--
-- Volcado de datos para la tabla `detalle_ctacte`
--

INSERT INTO `detalle_ctacte` (`id_detallectacte`, `id_ctacte`, `numero_cuota`, `vencimiento_cuota`, `valor_cuota`, `valor_cancelado`, `finalizado`) VALUES
(54, 2, 1, '2019-07-15', 287.5, 287.5, 0),
(55, 2, 2, '2019-08-15', 287.5, 0, 0),
(56, 2, 3, '2019-09-15', 287.5, 0, 0),
(57, 2, 4, '2019-10-15', 287.5, 0, 0),
(58, 2, 5, '2019-11-15', 287.5, 0, 0),
(59, 2, 6, '2019-12-15', 287.5, 0, 0),
(60, 2, 7, '2020-01-15', 287.5, 0, 0),
(61, 2, 8, '2020-02-15', 287.5, 0, 0),
(62, 2, 9, '2020-03-15', 287.5, 0, 0),
(63, 2, 10, '2020-04-15', 287.5, 0, 0),
(64, 3, 1, '2019-07-15', 221.667, 0, 0),
(65, 3, 2, '2019-08-15', 221.667, 0, 0),
(66, 3, 3, '2019-09-15', 221.667, 0, 0),
(67, 3, 4, '2019-10-15', 221.667, 0, 0),
(68, 3, 5, '2019-11-15', 221.667, 0, 0),
(69, 3, 6, '2019-12-15', 221.667, 0, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle_cuenta`
--

CREATE TABLE IF NOT EXISTS `detalle_cuenta` (
  `id_ctacte` int(11) NOT NULL AUTO_INCREMENT,
  `id_cliente` int(11) NOT NULL,
  `total` float NOT NULL,
  PRIMARY KEY (`id_ctacte`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `facturacion_boleta`
--

CREATE TABLE IF NOT EXISTS `facturacion_boleta` (
  `id_facturacionboleta` int(11) NOT NULL AUTO_INCREMENT,
  `id_boleta` int(11) NOT NULL,
  `id_condvta` varchar(10) NOT NULL,
  `cant_cuotas` int(11) NOT NULL,
  `monto` float NOT NULL,
  PRIMARY KEY (`id_facturacionboleta`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=57 ;

--
-- Volcado de datos para la tabla `facturacion_boleta`
--

INSERT INTO `facturacion_boleta` (`id_facturacionboleta`, `id_boleta`, `id_condvta`, `cant_cuotas`, `monto`) VALUES
(27, 16, '9', 0, 675),
(26, 15, '1', 0, 140),
(25, 15, '5', 0, 1000),
(24, 14, '4', 0, 525),
(23, 13, '1', 0, 525),
(22, 12, '1', 0, 4590),
(21, 11, '8', 0, 750),
(20, 10, '6', 6, 2570),
(19, 10, '1', 0, 2500),
(28, 17, '9', 6, 1520),
(29, 18, '9', 6, 675),
(30, 19, '9', 6, 1140),
(31, 20, '9', 6, 675),
(32, 21, '9', 6, 360),
(33, 22, '9', 6, 2800),
(34, 23, '9', 0, 1330),
(35, 24, '9', 6, 1710),
(36, 25, '9', 6, 675),
(37, 26, '9', 6, 1900),
(38, 27, '9', 6, 225),
(39, 28, '9', 12, 1680),
(40, 29, '9', 10, 2520),
(41, 30, '9', 10, 750),
(42, 31, '9', 10, 225),
(43, 32, '9', 10, 2240),
(44, 33, '9', 10, 225),
(45, 34, '9', 10, 500),
(46, 34, '1', 0, 250),
(47, 35, '9', 6, 360),
(48, 36, '9', 6, 225),
(49, 37, '9', 6, 675),
(50, 38, '1', 0, 225),
(51, 39, '1', 0, 250),
(52, 40, '9', 10, 1330),
(53, 41, '9', 10, 525),
(54, 42, '1', 0, 1000),
(55, 42, '9', 10, 2875),
(56, 43, '9', 6, 1330);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `localidad`
--

CREATE TABLE IF NOT EXISTS `localidad` (
  `id_localidad` int(11) NOT NULL AUTO_INCREMENT,
  `localidad` varchar(20) NOT NULL,
  PRIMARY KEY (`id_localidad`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `privilegio`
--

CREATE TABLE IF NOT EXISTS `privilegio` (
  `id_privilegio` int(11) NOT NULL AUTO_INCREMENT,
  `desc_privilegio` varchar(20) NOT NULL,
  PRIMARY KEY (`id_privilegio`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `privilegio`
--

INSERT INTO `privilegio` (`id_privilegio`, `desc_privilegio`) VALUES
(1, 'Administrador'),
(2, 'Vendedor');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `talle`
--

CREATE TABLE IF NOT EXISTS `talle` (
  `id_talle` int(11) NOT NULL AUTO_INCREMENT,
  `desc_talle` varchar(20) NOT NULL,
  PRIMARY KEY (`id_talle`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Volcado de datos para la tabla `talle`
--

INSERT INTO `talle` (`id_talle`, `desc_talle`) VALUES
(1, 'S'),
(2, 'M'),
(5, 'L'),
(6, 'XL'),
(9, 'Unico'),
(10, '34');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo`
--

CREATE TABLE IF NOT EXISTS `tipo` (
  `desc_tipo` varchar(20) NOT NULL,
  `id_tipo` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id_tipo`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=19 ;

--
-- Volcado de datos para la tabla `tipo`
--

INSERT INTO `tipo` (`desc_tipo`, `id_tipo`) VALUES
('Vestido Largo', 17),
('Pollera', 16),
('Buzo', 15),
('Remera', 14);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE IF NOT EXISTS `usuario` (
  `id_usuario` int(11) NOT NULL AUTO_INCREMENT,
  `cuenta_usuario` varchar(20) NOT NULL,
  `contraseña_usuario` varchar(20) NOT NULL,
  `nombre_usuario` varchar(20) NOT NULL,
  `apellido_usuario` varchar(20) NOT NULL,
  `id_privilegio` int(11) NOT NULL,
  PRIMARY KEY (`id_usuario`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`id_usuario`, `cuenta_usuario`, `contraseña_usuario`, `nombre_usuario`, `apellido_usuario`, `id_privilegio`) VALUES
(1, 'admin', 'mariabonita', 'Administrador', 'Administrador', 1),
(2, 'rodrimayer', 'rodri0104', 'Rodrigo', 'Mayer', 2);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
