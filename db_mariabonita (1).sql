-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 28-05-2019 a las 22:14:09
-- Versión del servidor: 5.5.24-log
-- Versión de PHP: 5.4.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `db_mariabonita`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `articulo`
--

CREATE TABLE IF NOT EXISTS `articulo` (
  `codigo` varchar(8) NOT NULL,
  `id_tipo` int(11) NOT NULL,
  `id_talle` int(11) NOT NULL,
  `precio_costo` float DEFAULT NULL,
  `precio_venta` float DEFAULT NULL,
  `stock` int(11) DEFAULT NULL,
  `desc_articulo` varchar(50) NOT NULL,
  PRIMARY KEY (`codigo`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `articulo`
--

INSERT INTO `articulo` (`codigo`, `id_tipo`, `id_talle`, `precio_costo`, `precio_venta`, `stock`, `desc_articulo`) VALUES
('RS-345', 17, 2, 1400, 2800, 1, 'Vestido de Fiesta Negro'),
('RX-22', 14, 9, 150, 250, 1, 'Remera cuello en V Blanca');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `boleta`
--

CREATE TABLE IF NOT EXISTS `boleta` (
  `id_boleta` int(11) NOT NULL,
  `fecha_boleta` date NOT NULL,
  `id_cliente` int(11) NOT NULL,
  `id_detalleboleta` int(11) NOT NULL,
  PRIMARY KEY (`id_boleta`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `caja_diaria`
--

CREATE TABLE IF NOT EXISTS `caja_diaria` (
  `id_caja` int(11) NOT NULL AUTO_INCREMENT,
  `fecha_caja` date NOT NULL,
  PRIMARY KEY (`id_caja`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliente`
--

CREATE TABLE IF NOT EXISTS `cliente` (
  `id_cliente` int(11) NOT NULL,
  `nombre_cliente` varchar(20) NOT NULL,
  `apellido_cliente` varchar(20) NOT NULL,
  `direccion` varchar(30) NOT NULL,
  `localidad` varchar(20) NOT NULL,
  `telefono` varchar(20) NOT NULL,
  `mail` varchar(30) NOT NULL,
  PRIMARY KEY (`id_cliente`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `cliente`
--

INSERT INTO `cliente` (`id_cliente`, `nombre_cliente`, `apellido_cliente`, `direccion`, `localidad`, `telefono`, `mail`) VALUES
(37305672, 'Rodri', 'Mayer', 'Maipu', 'LGSM', '3886525523', 'rodrimmayer@gmail.com'),
(1234555, 'Martin', 'Perez', 'Juan XXIII', 'LGSM', '123213123', 'adssadad'),
(12321455, 'Maria Agostina', 'Rubio', 'Barrio Santa Rosa', 'LGSM', '12312552', 'sadsadsad');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `color`
--

CREATE TABLE IF NOT EXISTS `color` (
  `id_color` int(11) NOT NULL,
  `desc_color` varchar(20) NOT NULL,
  PRIMARY KEY (`id_color`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cond_vta`
--

CREATE TABLE IF NOT EXISTS `cond_vta` (
  `id_condvta` int(11) NOT NULL,
  `desc_condvta` varchar(20) NOT NULL,
  PRIMARY KEY (`id_condvta`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cta_cte`
--

CREATE TABLE IF NOT EXISTS `cta_cte` (
  `id_ctacte` int(11) NOT NULL,
  `id_cliente` int(11) NOT NULL,
  `deuda` float NOT NULL,
  PRIMARY KEY (`id_ctacte`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle_boleta`
--

CREATE TABLE IF NOT EXISTS `detalle_boleta` (
  `id_detalleboleta` int(11) NOT NULL,
  `id_boleta` int(11) NOT NULL,
  `codigo` varchar(20) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `precio_total` float NOT NULL,
  PRIMARY KEY (`id_detalleboleta`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle_caja`
--

CREATE TABLE IF NOT EXISTS `detalle_caja` (
  `id_detallecaja` int(11) NOT NULL AUTO_INCREMENT,
  `id_caja` int(11) NOT NULL,
  `id_condvta` int(11) NOT NULL,
  `total` float NOT NULL,
  PRIMARY KEY (`id_detallecaja`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle_cuenta`
--

CREATE TABLE IF NOT EXISTS `detalle_cuenta` (
  `id_ctacte` int(11) NOT NULL AUTO_INCREMENT,
  `id_cliente` int(11) NOT NULL,
  `total` float NOT NULL,
  PRIMARY KEY (`id_ctacte`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `facturacion_boleta`
--

CREATE TABLE IF NOT EXISTS `facturacion_boleta` (
  `id_facturacionboleta` int(11) NOT NULL AUTO_INCREMENT,
  `id_boleta` int(11) NOT NULL,
  `id_condvta` varchar(10) NOT NULL,
  `monto` float NOT NULL,
  PRIMARY KEY (`id_facturacionboleta`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `localidad`
--

CREATE TABLE IF NOT EXISTS `localidad` (
  `id_localidad` int(11) NOT NULL AUTO_INCREMENT,
  `localidad` varchar(20) NOT NULL,
  PRIMARY KEY (`id_localidad`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `talle`
--

CREATE TABLE IF NOT EXISTS `talle` (
  `id_talle` int(11) NOT NULL AUTO_INCREMENT,
  `desc_talle` varchar(20) NOT NULL,
  PRIMARY KEY (`id_talle`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Volcado de datos para la tabla `talle`
--

INSERT INTO `talle` (`id_talle`, `desc_talle`) VALUES
(1, 'S'),
(2, 'M'),
(5, 'L'),
(6, 'XL'),
(9, 'Unico'),
(10, '34');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo`
--

CREATE TABLE IF NOT EXISTS `tipo` (
  `desc_tipo` varchar(20) NOT NULL,
  `id_tipo` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id_tipo`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18 ;

--
-- Volcado de datos para la tabla `tipo`
--

INSERT INTO `tipo` (`desc_tipo`, `id_tipo`) VALUES
('Vestido Largo', 17),
('Pollera', 16),
('Buzo', 15),
('Remera', 14);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
