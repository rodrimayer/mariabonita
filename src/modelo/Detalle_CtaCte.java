/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author rmmayer
 */
public class Detalle_CtaCte {

    private int id_ctacte;
    private int id_cliente;
    private int numero_cuota;
    private String vencimiento_cuota;
    private float valor_cuota;
    private float valor_cancelado;
    private int finalizado;

    public int getId_ctacte() {
        return id_ctacte;
    }

    public void setId_ctacte(int id_ctacte) {
        this.id_ctacte = id_ctacte;
    }

    public int getId_cliente() {
        return id_cliente;
    }

    public void setId_cliente(int id_cliente) {
        this.id_cliente = id_cliente;
    }

    public int getNumero_cuota() {
        return numero_cuota;
    }

    public void setNumero_cuota(int numero_cuota) {
        this.numero_cuota = numero_cuota;
    }

    public float getValor_cuota() {
        return valor_cuota;
    }

    public void setValor_cuota(float valor_cuota) {
        this.valor_cuota = valor_cuota;
    }

    public String getVencimiento_cuota() {
        return vencimiento_cuota;
    }

    public void setVencimiento_cuota(String vencimiento_cuota) {
        this.vencimiento_cuota = vencimiento_cuota;
    }

    public float getValor_cancelado() {
        return valor_cancelado;
    }

    public void setValor_cancelado(float valor_cancelado) {
        this.valor_cancelado = valor_cancelado;
    }

    public int getFinalizado() {
        return finalizado;
    }

    public void setFinalizado(int finalizado) {
        this.finalizado = finalizado;
    }

}
