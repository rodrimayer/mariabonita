/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author rmmayer
 */
public class Privilegio {
    
    private int id_privilegio;
    private String desc_privilegio;

    public int getId_privilegio() {
        return id_privilegio;
    }

    public void setId_privilegio(int id_privilegio) {
        this.id_privilegio = id_privilegio;
    }

    public String getDesc_privilegio() {
        return desc_privilegio;
    }

    public void setDesc_privilegio(String desc_privilegio) {
        this.desc_privilegio = desc_privilegio;
    }
    
    
    
}
