/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author Rodri Mayer
 */
public class Cond_vta {
    
    private int id_condvta;
    private String desc_condvta;
    private int permite_cuotas;

    public int getId_condvta() {
        return id_condvta;
    }

    public void setId_condvta(int id_condvta) {
        this.id_condvta = id_condvta;
    }

    public String getDesc_condvta() {
        return desc_condvta;
    }

    public void setDesc_condvta(String desc_condvta) {
        this.desc_condvta = desc_condvta;
    }

    public int getPermite_cuotas() {
        return permite_cuotas;
    }

    public void setPermite_cuotas(int permite_cuotas) {
        this.permite_cuotas = permite_cuotas;
    }
    
    
    
    
}
