/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author Rodri Mayer
 */
public class Detalle_boleta {
    
    private int id_detalleboleta;
    private int id_boleta;
    private String codigo;
    private int descuento;
    private float precio;

    public int getId_detalleboleta() {
        return id_detalleboleta;
    }

    public void setId_detalleboleta(int id_detalleboleta) {
        this.id_detalleboleta = id_detalleboleta;
    }

    public int getId_boleta() {
        return id_boleta;
    }

    public void setId_boleta(int id_boleta) {
        this.id_boleta = id_boleta;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public int getDescuento() {
        return descuento;
    }

    public void setDescuento(int descuento) {
        this.descuento = descuento;
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }
    
    
    
    
}
