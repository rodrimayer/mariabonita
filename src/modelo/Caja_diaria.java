/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author Rodri Mayer
 */
public class Caja_diaria {
    
    private int id_caja;
    private int fecha_caja;

    public int getId_caja() {
        return id_caja;
    }

    public void setId_caja(int id_caja) {
        this.id_caja = id_caja;
    }

    public int getFecha_caja() {
        return fecha_caja;
    }

    public void setFecha_caja(int fecha_caja) {
        this.fecha_caja = fecha_caja;
    }
    
    
    
}
