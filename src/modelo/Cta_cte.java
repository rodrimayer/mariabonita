/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author Rodri Mayer
 */
public class Cta_cte {

    private int cta_cte;
    private int id_cliente;

    public int getCta_cte() {
        return cta_cte;
    }

    public void setCta_cte(int cta_cte) {
        this.cta_cte = cta_cte;
    }

    public int getId_cliente() {
        return id_cliente;
    }

    public void setId_cliente(int id_cliente) {
        this.id_cliente = id_cliente;
    }

}
