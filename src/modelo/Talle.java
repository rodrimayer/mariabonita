/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author Rodri Mayer
 */
public class Talle {
    
    private int id_talle;
    private String desc_talle;

    public int getId_talle() {
        return id_talle;
    }

    public void setId_talle(int id_talle) {
        this.id_talle = id_talle;
    }

    public String getDesc_talle() {
        return desc_talle;
    }

    public void setDesc_talle(String desc_talle) {
        this.desc_talle = desc_talle;
    }
    
    
    
}
