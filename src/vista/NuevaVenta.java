/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package vista;

import conexion.conexionDB;
import controlador.*;
import modelo.Cliente;
import modelo.Articulo;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ButtonGroup;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;
import modelo.Boleta;
import modelo.Cta_cte;
import modelo.Detalle_CtaCte;
import modelo.Detalle_boleta;
import modelo.Facturacion_boleta;
import modelo.Talle;
import modelo.Usuario;
import vista.Inicio.ModeloTablas;

/**
 *
 * @author Administrador
 */
public class NuevaVenta extends javax.swing.JFrame {

    /**
     * Creates new form CargarPedidoDidactico
     */
    manejadorCliente mc = new manejadorCliente();
    manejadorArticulos manA = new manejadorArticulos();
    manejadorTablas mt = new manejadorTablas();
    manejadorFechas mf = new manejadorFechas();
    manejadorFacturacion mfac = new manejadorFacturacion();
    manejadorCtaCte mcc = new manejadorCtaCte();
    manejadorImpresion mi = new manejadorImpresion();

    String codigo;
    Cliente cli = new Cliente();
    Articulo m = new Articulo();
    Boleta bo = new Boleta();
    Cta_cte cc = new Cta_cte();
    Detalle_boleta db = new Detalle_boleta();
    Facturacion_boleta dc = new Facturacion_boleta();
    Detalle_CtaCte dcc = new Detalle_CtaCte();
    Talle t = new Talle();
    static ArrayList<Detalle_boleta> aDB = new ArrayList();
    JTable tablaBoletasSinCerrar;
    JFrame inicio;
    boolean primerNumero = true;
    boolean start = true;
    boolean bandera = true;
    int iB = 1;
    boolean aceptar = true;

    public class MiModelo extends DefaultTableModel {

        public boolean isCellEditable(int row, int column) {
            return false;
        }
    }

    public class ModeloTablaArticulosVenta extends DefaultTableModel {

        public boolean isCellEditable(int row, int column) {
            return false;
        }
    }

    public class ModeloTablaFacturacion extends DefaultTableModel {

        public boolean isCellEditable(int row, int column) {
            return false;
        }
    }

    //MiModelo modelo = new MiModelo();
    ModeloTablaArticulosVenta modeloTablaResumenPedido = new ModeloTablaArticulosVenta();
    ModeloTablaFacturacion modeloTablaFacturacion = new ModeloTablaFacturacion();
    ModeloTablas modeloTablaVentasDelDia;

    int id_pedido;

    ButtonGroup group = new ButtonGroup();
    
    Usuario user = new Usuario();

    public NuevaVenta(JTable tablaVentasDelDia, JTable tablaDeudores, ModeloTablas modelo, ModeloTablas modeloDeudores, Usuario u, JFrame ini) {
        initComponents();
        this.setTitle("NUEVA VENTA");
        user = u;
        inicio = ini;

        this.modeloTablaVentasDelDia = modelo;

        Toolkit t = Toolkit.getDefaultToolkit();
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        Dimension frameS = new Dimension(910, 740);
        Dimension panelResumenPedidoSize = new Dimension(800, 220);
        this.setSize(frameS);
        this.setResizable(false);

        this.panelResumenPedido.setSize(800, 220);
        this.panelResumenPedido.setPreferredSize(panelResumenPedidoSize);

        labelCliente.setText("");
        labelDireccion.setText("");
        labelTelefono.setText("");
        labelDNI.setText("");
        this.botonAgregarFormaPago.setEnabled(false);
        this.tablaBoletasSinCerrar = tablaVentasDelDia;
        this.botonConfirmarPedido.setEnabled(false);
        this.botonEliminarFormaDePago.setEnabled(false);
        //this.txtDeudaCtaCte.setEnabled(false);
        final metodoBusquedaCliente mb = new metodoBusquedaCliente(this);
        final metodoBusquedaArticulo ma = new metodoBusquedaArticulo(this);

        group.add(check10);
        group.add(check20);
        group.add(check30);
        group.add(check40);

        mt.diseñoTablaArticulosVenta(tablaResumenVenta, modeloTablaResumenPedido);
        mt.diseñoTablaFacturacion(tablaFacturacion, modeloTablaFacturacion);

        manA.traerArticulosComboBox(comboBoxDetalleArticulo);

        modeloTablaFacturacion.addTableModelListener(new TableModelListener() {

            public void tableChanged(TableModelEvent e) {
                System.out.println("SE MODIFICO LA TABLA");
                float montoAgregado = 0;
                float restante = Float.parseFloat(txtTotalAcumulado.getText());
                for (int i = 0; i < tablaFacturacion.getRowCount(); i++) {
                    montoAgregado = Float.parseFloat(tablaFacturacion.getValueAt(i, 3).toString());
                    restante = restante - montoAgregado;
                    txtTotalRestante.setText(String.valueOf(restante));
                    if (restante == 0.0) {
                        botonGuardarPedido.setEnabled(true);
                    } else {
                        botonGuardarPedido.setEnabled(false);
                    }

                }
                System.out.println("TOTAL RESTANTE: " + restante);

            }
        });

        modeloTablaResumenPedido.addTableModelListener(new TableModelListener() {

            public void tableChanged(TableModelEvent e) {
                System.out.println("SE MODIFICO LA TABLA DE ARTICULOS");
                float montoAgregado = 0;
                float acumulado = 0;
                float pagado = 0;
                float restante = 0;

                if (tablaResumenVenta.getRowCount() == 0) {
                    botonConfirmarPedido.setEnabled(false);
                } else {
                    botonConfirmarPedido.setEnabled(true);
                    for (int i = 0; i < tablaResumenVenta.getRowCount(); i++) {
                        montoAgregado = Float.parseFloat(tablaResumenVenta.getValueAt(i, 5).toString());
                        acumulado = acumulado + montoAgregado;
                    }

                    txtTotalAcumulado.setText(String.valueOf(acumulado));

                    for (int i = 0; i < tablaFacturacion.getRowCount(); i++) {
                        pagado = pagado + Float.parseFloat(tablaFacturacion.getValueAt(i, 3).toString());
                    }
                    restante = acumulado - pagado;
                    System.out.println("TOTAL PAGADO: " + pagado);
                    System.out.println("TOTAL RESTANTE: " + restante);
                    txtTotalRestante.setText(String.valueOf(restante));

                    if (restante == 0.0) {
                        botonGuardarPedido.setEnabled(true);
                    } else {
                        botonGuardarPedido.setEnabled(false);
                    }
                }

            }
        });

        //labelCliente.setForeground(Color.GREEN);
        //labelDireccion.setForeground(Color.GREEN);
        //labelLocalidad.setForeground(Color.GREEN);
        botonEditarCliente.setEnabled(false);

        botonEliminarMercaderia.setEnabled(false);

        Dimension d = new Dimension(506, 121);

        panelResumenPedido.setMaximumSize(d);
        panelResumenPedido.setMinimumSize(d);
        panelResumenPedido.setSize(d);
        panelResumenPedido.setBounds(panelResumenPedido.getX(), panelResumenPedido.getY(), 506, 121);
        panelResumenPedido.setPreferredSize(d);

        comboBoxDetalleArticulo.setEnabled(true);

        botonGuardarPedido.setEnabled(false);

        mc.traerClientesComboBox(comboBoxNombreCliente);
        start = false;

        this.comboBoxNombreCliente.getEditor().getEditorComponent().addKeyListener(new KeyAdapter() {

            @Override
            public void keyReleased(KeyEvent evt) {

                String campo;
                String cadenaEscrita = comboBoxNombreCliente.getEditor().getItem().toString();

                //System.out.println(cadenaEscrita);
                campo = "nombre";

                if (evt.getKeyChar() == KeyEvent.VK_ENTER || evt.getKeyChar() == KeyEvent.VK_TAB) {
                    //JOptionPane.showMessageDialog(null, "APRETO tab");
                    if (mb.comparar(cadenaEscrita)) {// compara si el texto escrito se encuentra en la lista
                        // busca el texto escrito en la base de datos                      

                        //mc.obtenerCliente(cadenaEscrita, cli);
                        txtCodigoArticulo.requestFocus();

                    } else {// en caso contrario toma como default el elemento 0 o sea el primero de la lista y lo busca.
                        if (comboBoxNombreCliente.getItemAt(0) != null) {

                            //mc.obtenerCliente(cadenaEscrita, cli);
                            comboBoxNombreCliente.setSelectedIndex(0);
                            txtCodigoArticulo.requestFocus();
                        }

                    }

                    comboBoxNombreCliente.setEditable(false);
                    comboBoxNombreCliente.setEnabled(false);
                    botonEditarCliente.setEnabled(true);

                }

                mc.obtenerCliente(cadenaEscrita, cli);

                if (mcc.existeCtaCte(cli.getId_cliente())) {
                    cc = mcc.TraerCtaCte(cli.getId_cliente());
                    txtDeudaCtaCte.setText("$ " + String.valueOf(mcc.deudaCtaCte(cc.getCta_cte())));
                }

                labelCliente.setText(cli.getApellido_cliente() + " " + cli.getNombre_cliente());
                labelDireccion.setText(cli.getDireccion() + ", " + cli.getLocalidad());
                labelDNI.setText(String.valueOf(cli.getId_cliente()));
                labelTelefono.setText(cli.getTelefono());

                if (evt.getKeyCode() >= 65 && evt.getKeyCode() <= 90 || evt.getKeyCode() >= 96 && evt.getKeyCode() <= 105 || evt.getKeyCode() == 8) {

                    comboBoxNombreCliente.setModel(mb.getLista(cadenaEscrita, campo));

                    if (comboBoxNombreCliente.getItemCount() > 0) {
                        comboBoxNombreCliente.getEditor().setItem(cadenaEscrita);
                        comboBoxNombreCliente.showPopup();

                    } else {
                        comboBoxNombreCliente.addItem(cadenaEscrita);
                    }

                    labelCliente.setText("");
                    labelDireccion.setText("");
                    labelDNI.setText("");
                    labelTelefono.setText("");
                }

            }
        });

        this.comboBoxDetalleArticulo.getEditor().getEditorComponent().addKeyListener(new KeyAdapter() {

            public void keyReleased(KeyEvent evt) {

                String campo;
                String cadenaEscrita = comboBoxDetalleArticulo.getEditor().getItem().toString();

                System.out.println(cadenaEscrita);
                campo = "desc_articulo";

                if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
                    if (ma.comparar(cadenaEscrita)) {// compara si el texto escrito se encuentra en la lista
                        // busca el texto escrito en la base de datos                      

                        //cadenaEscrita = comboBoxDetalleArticulo.getEditor().getItem().toString();
                        m = manA.obtenerArticuloPorString(cadenaEscrita);
                        //comboBoxDetalleArticulo.requestFocus();
                        //mt.agregarArticuloVenta(tablaResumenVenta, modeloTablaResumenPedido ,m);
                        System.out.println("ENTRO 1");
                        System.out.println("ELEMENTO SELECCIONADO: " + comboBoxDetalleArticulo.getEditor().getItem().toString());
                        String comparar = comboBoxDetalleArticulo.getEditor().getItem().toString();

                        botonAceptarArticulo.setVisible(true);

                    } else {// en caso contrario toma como default el elemento 0 o sea el primero de la lista y lo busca.
                        if (comboBoxDetalleArticulo.getItemAt(0) != null) {

                            comboBoxDetalleArticulo.setSelectedIndex(0);
                            cadenaEscrita = comboBoxDetalleArticulo.getItemAt(0).toString();
                            m = manA.obtenerArticuloPorString(cadenaEscrita);
                            System.out.println("ENTRO 2");
                            System.out.println("ELEMENTO SELECCIONADO: " + comboBoxDetalleArticulo.getEditor().getItem().toString());
                            String comparar = comboBoxDetalleArticulo.getItemAt(0).toString();
                        }

                    }
                    txtCodigoArticulo.setEnabled(true);
                    txtCodigoArticulo.requestFocus();
                    int descuento = 0;

                    if (check10.isSelected()) {
                        descuento = 10;
                    } else if (check20.isSelected()) {
                        descuento = 20;
                    } else if (check30.isSelected()) {
                        descuento = 30;
                    } else if (check40.isSelected()) {
                        descuento = 40;
                    }

                    mt.agregarArticuloVenta(tablaResumenVenta, modeloTablaResumenPedido, m, descuento);
                    //txtTotalAcumulado.setText(String.valueOf(Float.parseFloat(txtTotalAcumulado.getText()) + m.getPrecio_venta()));
                    //txtTotalRestante.setText(txtTotalAcumulado.getText());
                    comboBoxDetalleArticulo.removeAllItems();
                    manA.traerArticulosComboBox(comboBoxDetalleArticulo);
                    //botonAgregarFormaPago.setEnabled(true);
                    botonConfirmarPedido.setEnabled(true);

                }

                if (evt.getKeyCode() >= 65 && evt.getKeyCode() <= 90 || evt.getKeyCode() >= 96 && evt.getKeyCode() <= 105 || evt.getKeyCode() == 8) {

                    comboBoxDetalleArticulo.setModel(ma.getLista(cadenaEscrita, campo));
                    txtCodigoArticulo.setText("");

                    if (comboBoxDetalleArticulo.getEditor().getItem().toString().equals("")) {
                        //System.out.println("Esta vacio");
                        txtCodigoArticulo.setEnabled(true);
                    } else {
                        txtCodigoArticulo.setEnabled(false);
                        //System.out.println("Esta escribiendo");
                    }

                    if (comboBoxDetalleArticulo.getItemCount() > 0) {
                        comboBoxDetalleArticulo.getEditor().setItem(cadenaEscrita);
                        comboBoxDetalleArticulo.showPopup();

                    } else {
                        comboBoxDetalleArticulo.addItem(cadenaEscrita);
                    }
                }

            }
        });

    }

    public JComboBox getcomboBoxNombreCliente() {
        return comboBoxNombreCliente;
    }

    public void setcomboBoxNombreCliente(JComboBox comboBoxNombreCliente) {
        this.comboBoxNombreCliente = comboBoxNombreCliente;
    }

    public JComboBox getcomboBoxMercaderia() {
        return comboBoxDetalleArticulo;
    }

    public void setcomboBoxMercaderia(JComboBox comboBoxMercaderia) {
        this.comboBoxDetalleArticulo = comboBoxMercaderia;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        panelCliente = new javax.swing.JPanel();
        comboBoxNombreCliente = new javax.swing.JComboBox();
        jLabel5 = new javax.swing.JLabel();
        botonEditarCliente = new javax.swing.JButton();
        panelMercaderia = new javax.swing.JPanel();
        comboBoxDetalleArticulo = new javax.swing.JComboBox();
        jLabel2 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        txtCodigoArticulo = new javax.swing.JTextField();
        botonAceptarArticulo = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        labelDireccion = new javax.swing.JLabel();
        labelCliente = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        labelDNI = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        labelTelefono = new javax.swing.JLabel();
        panelResumenPedido = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tablaResumenVenta = new javax.swing.JTable();
        botonEliminarMercaderia = new javax.swing.JButton();
        jLabel12 = new javax.swing.JLabel();
        txtTotalAcumulado = new javax.swing.JLabel();
        botonConfirmarPedido = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txtDeudaCtaCte = new javax.swing.JTextField();
        jPanel4 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tablaFacturacion = new javax.swing.JTable();
        jLabel13 = new javax.swing.JLabel();
        txtTotalRestante = new javax.swing.JLabel();
        botonAgregarFormaPago = new javax.swing.JButton();
        botonEliminarFormaDePago = new javax.swing.JButton();
        botonGuardarPedido = new javax.swing.JButton();
        jPanel5 = new javax.swing.JPanel();
        check40 = new javax.swing.JCheckBox();
        check30 = new javax.swing.JCheckBox();
        check20 = new javax.swing.JCheckBox();
        check10 = new javax.swing.JCheckBox();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setResizable(false);

        panelCliente.setBorder(javax.swing.BorderFactory.createTitledBorder("Cliente"));

        comboBoxNombreCliente.setEditable(true);
        comboBoxNombreCliente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboBoxNombreClienteActionPerformed(evt);
            }
        });

        jLabel5.setText("Seleccione Cliente:");

        botonEditarCliente.setText("EDITAR CLIENTE");
        botonEditarCliente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonEditarClienteActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panelClienteLayout = new javax.swing.GroupLayout(panelCliente);
        panelCliente.setLayout(panelClienteLayout);
        panelClienteLayout.setHorizontalGroup(
            panelClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelClienteLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(panelClienteLayout.createSequentialGroup()
                        .addComponent(jLabel5)
                        .addGap(180, 180, 180))
                    .addGroup(panelClienteLayout.createSequentialGroup()
                        .addComponent(comboBoxNombreCliente, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(botonEditarCliente)
                        .addGap(10, 10, 10))))
        );
        panelClienteLayout.setVerticalGroup(
            panelClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelClienteLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel5)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(panelClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(comboBoxNombreCliente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(botonEditarCliente))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        panelMercaderia.setBorder(javax.swing.BorderFactory.createTitledBorder("Artículo"));

        comboBoxDetalleArticulo.setEditable(true);
        comboBoxDetalleArticulo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboBoxDetalleArticuloActionPerformed(evt);
            }
        });

        jLabel2.setText("Código:");

        jLabel6.setText("Detalle:");

        txtCodigoArticulo.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtCodigoArticuloKeyReleased(evt);
            }
        });

        botonAceptarArticulo.setText("ACEPTAR ARTICULO");
        botonAceptarArticulo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonAceptarArticuloActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panelMercaderiaLayout = new javax.swing.GroupLayout(panelMercaderia);
        panelMercaderia.setLayout(panelMercaderiaLayout);
        panelMercaderiaLayout.setHorizontalGroup(
            panelMercaderiaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelMercaderiaLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(txtCodigoArticulo, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel6)
                .addGap(32, 32, 32)
                .addComponent(comboBoxDetalleArticulo, 0, 271, Short.MAX_VALUE)
                .addGap(39, 39, 39)
                .addComponent(botonAceptarArticulo)
                .addContainerGap())
        );
        panelMercaderiaLayout.setVerticalGroup(
            panelMercaderiaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelMercaderiaLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelMercaderiaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(botonAceptarArticulo, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(comboBoxDetalleArticulo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6)
                    .addComponent(jLabel2)
                    .addComponent(txtCodigoArticulo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Datos del Cliente"));

        jLabel7.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        jLabel7.setText("CLIENTE SELECCIONADO:");

        jLabel8.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        jLabel8.setText("DIRECCION:");

        labelDireccion.setFont(new java.awt.Font("Tahoma", 1, 10)); // NOI18N
        labelDireccion.setText("jLabel10");

        labelCliente.setFont(new java.awt.Font("Tahoma", 1, 10)); // NOI18N
        labelCliente.setText("jlabel");
        labelCliente.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                labelClientePropertyChange(evt);
            }
        });

        jLabel11.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        jLabel11.setText("DNI:");

        labelDNI.setFont(new java.awt.Font("Tahoma", 1, 10)); // NOI18N
        labelDNI.setText("jLabel10");

        jLabel9.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        jLabel9.setText("TELÉFONO:");

        labelTelefono.setFont(new java.awt.Font("Tahoma", 1, 10)); // NOI18N
        labelTelefono.setText("jLabel10");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel7)
                    .addComponent(jLabel8)
                    .addComponent(jLabel11)
                    .addComponent(jLabel9))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(labelDNI, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(labelCliente, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(labelDireccion, javax.swing.GroupLayout.DEFAULT_SIZE, 198, Short.MAX_VALUE))
                                .addGap(0, 0, Short.MAX_VALUE)))
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(labelTelefono, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(113, Short.MAX_VALUE))))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(labelCliente)
                    .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 13, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(labelDireccion))
                .addGap(11, 11, 11)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel11)
                    .addComponent(labelDNI))
                .addGap(11, 11, 11)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jLabel9))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(labelTelefono)
                        .addGap(0, 0, Short.MAX_VALUE))))
        );

        panelResumenPedido.setBorder(javax.swing.BorderFactory.createTitledBorder("Resumen Pedido"));

        tablaResumenVenta.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null}
            },
            new String [] {
                "CODIGO", "ARTICULO", "PRECIO"
            }
        ));
        tablaResumenVenta.setMinimumSize(new java.awt.Dimension(225, 64));
        tablaResumenVenta.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tablaResumenVentaMouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(tablaResumenVenta);

        botonEliminarMercaderia.setText("ELIMINAR MERCADERIA");
        botonEliminarMercaderia.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonEliminarMercaderiaActionPerformed(evt);
            }
        });

        jLabel12.setText("TOTAL ACUMULADO:");

        txtTotalAcumulado.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        txtTotalAcumulado.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        txtTotalAcumulado.setText("0");

        botonConfirmarPedido.setText("CONFIRMAR PEDIDO");
        botonConfirmarPedido.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonConfirmarPedidoActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panelResumenPedidoLayout = new javax.swing.GroupLayout(panelResumenPedido);
        panelResumenPedido.setLayout(panelResumenPedidoLayout);
        panelResumenPedidoLayout.setHorizontalGroup(
            panelResumenPedidoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelResumenPedidoLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelResumenPedidoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelResumenPedidoLayout.createSequentialGroup()
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 829, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(panelResumenPedidoLayout.createSequentialGroup()
                        .addComponent(botonEliminarMercaderia, javax.swing.GroupLayout.PREFERRED_SIZE, 227, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(botonConfirmarPedido, javax.swing.GroupLayout.PREFERRED_SIZE, 227, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(47, 47, 47)
                        .addComponent(jLabel12)
                        .addGap(18, 18, 18)
                        .addComponent(txtTotalAcumulado, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(44, 44, 44))))
        );
        panelResumenPedidoLayout.setVerticalGroup(
            panelResumenPedidoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelResumenPedidoLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 112, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(17, 17, 17)
                .addGroup(panelResumenPedidoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(botonEliminarMercaderia)
                    .addComponent(botonConfirmarPedido)
                    .addComponent(jLabel12)
                    .addComponent(txtTotalAcumulado, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder("Cuenta Corriente"));

        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Deuda Acumulada");

        txtDeudaCtaCte.setEditable(false);
        txtDeudaCtaCte.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(txtDeudaCtaCte)
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addGap(18, 18, 18)
                .addComponent(txtDeudaCtaCte, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder("Facturacion"));

        tablaFacturacion.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "ID_Pago", "Detalle", "Cuotas", "Monto"
            }
        ));
        tablaFacturacion.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tablaFacturacionMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tablaFacturacion);

        jLabel13.setText("TOTAL RESTANTE:");

        txtTotalRestante.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        txtTotalRestante.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        txtTotalRestante.setText("0");

        botonAgregarFormaPago.setText("( + )");
        botonAgregarFormaPago.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonAgregarFormaPagoActionPerformed(evt);
            }
        });

        botonEliminarFormaDePago.setText("ELIMINAR FORMA DE PAGO");
        botonEliminarFormaDePago.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonEliminarFormaDePagoActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(botonAgregarFormaPago)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 829, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(botonEliminarFormaDePago, javax.swing.GroupLayout.PREFERRED_SIZE, 227, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel13)
                        .addGap(18, 18, 18)
                        .addComponent(txtTotalRestante, javax.swing.GroupLayout.PREFERRED_SIZE, 66, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(41, 41, 41))))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel13)
                        .addComponent(txtTotalRestante, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(botonAgregarFormaPago)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 88, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(botonEliminarFormaDePago)))
                .addGap(14, 14, 14))
        );

        botonGuardarPedido.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        botonGuardarPedido.setText("GUARDAR PEDIDO");
        botonGuardarPedido.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonGuardarPedidoActionPerformed(evt);
            }
        });

        jPanel5.setBorder(javax.swing.BorderFactory.createTitledBorder("Descuentos"));

        check40.setText("40%");

        check30.setText("30%");
        check30.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                check30ActionPerformed(evt);
            }
        });

        check20.setText("20%");

        check10.setText("10%");
        check10.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                check10ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(check10)
                    .addComponent(check30))
                .addGap(18, 18, 18)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(check40)
                    .addComponent(check20))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(check10)
                    .addComponent(check20))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 2, Short.MAX_VALUE)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(check30)
                    .addComponent(check40)))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(botonGuardarPedido, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel4, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(panelResumenPedido, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(panelMercaderia, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(panelCliente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, 154, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addContainerGap(25, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(panelCliente, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(panelMercaderia, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(panelResumenPedido, javax.swing.GroupLayout.PREFERRED_SIZE, 204, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(botonGuardarPedido, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void comboBoxNombreClienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboBoxNombreClienteActionPerformed
        // TODO add your handling code here:

        if (!start) {
            mc.obtenerCliente(comboBoxNombreCliente.getSelectedItem().toString(), cli);
            if (mcc.existeCtaCte(cli.getId_cliente())) {
                cc = mcc.TraerCtaCte(cli.getId_cliente());
                txtDeudaCtaCte.setText("$ " + String.valueOf(mcc.deudaCtaCte(cc.getCta_cte())));
            }
            String busqueda = this.comboBoxNombreCliente.getEditor().getItem().toString();
            labelCliente.setText(cli.getApellido_cliente() + " " + cli.getNombre_cliente());
            labelDireccion.setText(cli.getDireccion() + ", " + cli.getLocalidad());
            labelDNI.setText(String.valueOf(cli.getId_cliente()));
            labelTelefono.setText(cli.getTelefono());
            this.botonEditarCliente.setEnabled(true);
            this.comboBoxNombreCliente.setEnabled(false);
        }

    }//GEN-LAST:event_comboBoxNombreClienteActionPerformed

    private void botonGuardarPedidoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonGuardarPedidoActionPerformed
        // TODO add your handling code here:
        
        //ArrayList<Detalle_boleta> aDB = new ArrayList<Detalle_boleta>();

        bo.setCliente(cli);
        bo.setFecha_boleta(mf.obtenerFechaActual());
        bo.setTotal(Float.parseFloat(this.txtTotalAcumulado.getText()));

        mfac.insertarBoleta(bo, user);

        bo.setId_boleta(mfac.obtenerID_boleta());
        
        for (int i = 0; i < this.tablaResumenVenta.getRowCount(); i++) {
            
            String codigo = this.tablaResumenVenta.getValueAt(i, 0).toString();
            
            Detalle_boleta det_bol = new Detalle_boleta();

            int descuento = 0;
            if (!this.tablaResumenVenta.getValueAt(i, 4).toString().equals("-")) {
                descuento = Integer.parseInt(this.tablaResumenVenta.getValueAt(i, 4).toString());
            }

            float monto = Float.parseFloat(this.tablaResumenVenta.getValueAt(i, 5).toString());

            db.setId_boleta(bo.getId_boleta());
            det_bol.setId_boleta(bo.getId_boleta());
            db.setCodigo(codigo);
            det_bol.setCodigo(codigo);
            db.setDescuento(descuento);
            det_bol.setDescuento(descuento);
            db.setPrecio(monto);
            det_bol.setPrecio(monto);

            aDB.add(det_bol);            
            mfac.insertarDetallesBoleta(bo.getId_boleta(), db);
            //mi.imprimirDetalleFactura(db);
            //manA.actualizarStock(codigo);

        }
        
        for (int i = 0; i<aDB.size(); i++){
            System.out.println(aDB.get(i).getCodigo()+" "+aDB.get(i).getDescuento()+" "+aDB.get(i).getPrecio());
        }
        
        mi.imprimirFactura(bo, cli, user, aDB);

        for (int i = 0; i < this.tablaFacturacion.getRowCount(); i++) {
            int id_condvta = Integer.parseInt(this.tablaFacturacion.getValueAt(i, 0).toString());

            int cant_cuotas = 0;
            if (!this.tablaFacturacion.getValueAt(i, 2).toString().equals("")) {
                cant_cuotas = Integer.parseInt(this.tablaFacturacion.getValueAt(i, 2).toString());
            }

            float monto = Float.parseFloat(this.tablaFacturacion.getValueAt(i, 3).toString());

            dc.setId_boleta(bo.getId_boleta());
            dc.setId_condvta(id_condvta);
            dc.setCant_cuotas(cant_cuotas);
            dc.setTotal(monto);

            mfac.insertarFormasDePago(bo.getId_boleta(), dc);

            if (this.tablaFacturacion.getValueAt(i, 1).toString().equals("CUENTA CORRIENTE")) {
                System.out.println("INGRESA MONTO A CUENTA CORRIENTE");

                for (int j = 0; j < dc.getCant_cuotas(); j++) {
                    dcc.setId_cliente(cli.getId_cliente());
                    dcc.setId_ctacte(mcc.TraerCtaCte(cli.getId_cliente()).getCta_cte());
                    dcc.setValor_cuota(dc.getTotal() / dc.getCant_cuotas());
                    dcc.setVencimiento_cuota(mf.obtenerFechasVtoCtaCte(mf.obtenerFechaActual(), j + 1));
                    dcc.setNumero_cuota(j+1);
                    mcc.agregarMontoCuentaCorriente(dcc);
                    System.out.println("SE AGREGO CUOTA NUMERO: " + (j + 1));
                }

            }

        }
        
        JOptionPane.showMessageDialog(null, "BOLETA GUARDAD CON EXITO");
        
        inicio.dispose();
        Inicio ini2 = new Inicio(user);
        ini2.setVisible(true);
        ini2.setLocationRelativeTo(null);
        //mt.agregarVentasDelDia(tablaResumenVenta, modeloTablaVentasDelDia, bo);
        //mt.agregarDeudaCtaCte(tablaResumenVenta, modeloTablaVentasDelDia);
        this.dispose();


    }//GEN-LAST:event_botonGuardarPedidoActionPerformed

    private void tablaFacturacionMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tablaFacturacionMouseClicked
        // TODO add your handling code here:

        if (tablaFacturacion.getSelectedRowCount() == 1) {
            botonEliminarFormaDePago.setEnabled(true);

        } else {
            botonEliminarFormaDePago.setEnabled(false);
        }
    }//GEN-LAST:event_tablaFacturacionMouseClicked

    private void botonEditarClienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonEditarClienteActionPerformed
        // TODO add your handling code here:
        comboBoxNombreCliente.setEnabled(true);
        comboBoxNombreCliente.setEditable(true);
        botonEditarCliente.setEnabled(false);
        comboBoxNombreCliente.requestFocus();
        start = true;
        comboBoxNombreCliente.removeAllItems();
        mc.traerClientesComboBox(comboBoxNombreCliente);
        start = false;
        this.labelCliente.setText("");
        this.labelDNI.setText("");
        this.labelDireccion.setText("");
        this.labelTelefono.setText("");
        this.txtDeudaCtaCte.setText("");

    }//GEN-LAST:event_botonEditarClienteActionPerformed

    private void botonEliminarMercaderiaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonEliminarMercaderiaActionPerformed
        // TODO add your handling code here:

        int filaS = this.tablaResumenVenta.getSelectedRow();
        //int colS = this.tablaResumenVenta.getSelectedColumn();

        float monto = Float.parseFloat(this.tablaResumenVenta.getValueAt(filaS, 3).toString());
        float montoSd = Float.parseFloat(this.txtTotalAcumulado.getText());
        float montoRes = montoSd - monto;

        this.txtTotalAcumulado.setText(String.valueOf(montoRes));
        this.txtTotalRestante.setText(String.valueOf(montoRes));

        this.modeloTablaResumenPedido.removeRow(this.tablaResumenVenta.getSelectedRow());

        if (tablaResumenVenta.getRowCount() == 0) {
            botonGuardarPedido.setEnabled(false);
            this.botonAgregarFormaPago.setEnabled(false);
        }

        this.botonEliminarMercaderia.setEnabled(false);
        txtCodigoArticulo.requestFocus();
    }//GEN-LAST:event_botonEliminarMercaderiaActionPerformed

    private void labelClientePropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_labelClientePropertyChange
        // TODO add your handling code here
        /*
        * if (this.comboBoxNombreCliente.getSelectedIndex() != -1 &&
            * this.comboBoxNombreCliente.getSelectedItem().toString().equals(this.labelCliente.getText()))
        * { panelMercaderia.setVisible(true); panelCliente.setEnabled(false);
            * this.comboBoxNombreCliente.setEditable(false);
            * comboBoxNombreCliente.setEnabled(false);
            * this.comboBoxTipoMercaderia.setVisible(true);
            * this.comboBoxMercaderia.setVisible(true);
            * comboBoxTipoMercaderia.setEnabled(true);
            * comboBoxTipoMercaderia.setEditable(true);
            * //comboBoxMercaderia.setEnabled(true);
            * //comboBoxMercaderia.setEditable(true); }
         */
    }//GEN-LAST:event_labelClientePropertyChange

    private void tablaResumenVentaMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tablaResumenVentaMouseClicked
        // TODO add your handling code here:
        if (tablaResumenVenta.getSelectedRowCount() == 1 && this.botonConfirmarPedido.getText().equals("CONFIRMAR PEDIDO")) {
            botonEliminarMercaderia.setEnabled(true);

        } else {
            botonEliminarMercaderia.setEnabled(false);
        }
    }//GEN-LAST:event_tablaResumenVentaMouseClicked

    private void botonAgregarFormaPagoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonAgregarFormaPagoActionPerformed
        // TODO add your handling code here:
        AgregarPagoFacturacion apf = new AgregarPagoFacturacion(this.tablaFacturacion, this.modeloTablaFacturacion, Float.parseFloat(this.txtTotalRestante.getText()), this);
        apf.setLocationRelativeTo(null);
        apf.setVisible(true);
    }//GEN-LAST:event_botonAgregarFormaPagoActionPerformed

    private void botonEliminarFormaDePagoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonEliminarFormaDePagoActionPerformed
        // TODO add your handling code here:

        int filaEliminada = this.tablaFacturacion.getSelectedRow();
        float montoEliminado = Float.parseFloat(this.tablaFacturacion.getValueAt(filaEliminada, 3).toString());

        float nuevoMonto = Float.parseFloat(this.txtTotalRestante.getText()) + montoEliminado;

        this.txtTotalRestante.setText(String.valueOf(nuevoMonto));

        this.modeloTablaFacturacion.removeRow(filaEliminada);

        if (tablaFacturacion.getRowCount() == 0) {
            botonGuardarPedido.setEnabled(false);
        }
        this.botonEliminarFormaDePago.setEnabled(false);
    }//GEN-LAST:event_botonEliminarFormaDePagoActionPerformed

    private void botonConfirmarPedidoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonConfirmarPedidoActionPerformed
        // TODO add your handling code here:
        if (aceptar) {
            this.tablaResumenVenta.setEnabled(false);
            this.txtCodigoArticulo.setEnabled(false);
            this.comboBoxDetalleArticulo.setEnabled(false);
            this.botonAceptarArticulo.setEnabled(false);
            this.botonConfirmarPedido.setText("EDITAR PEDIDO");
            botonEliminarMercaderia.setEnabled(false);
            aceptar = false;
            this.tablaResumenVenta.setCellSelectionEnabled(false);
            this.botonAgregarFormaPago.setEnabled(true);
        } else {
            this.tablaResumenVenta.setEnabled(true);
            this.txtCodigoArticulo.setEnabled(true);
            this.comboBoxDetalleArticulo.setEnabled(true);
            this.botonAceptarArticulo.setEnabled(true);
            this.botonConfirmarPedido.setText("CONFIRMAR PEDIDO");
            this.botonGuardarPedido.setEnabled(false);
            this.botonAgregarFormaPago.setEnabled(false);            //botonEliminarMercaderia.setEnabled(true);
            aceptar = true;

        }

    }//GEN-LAST:event_botonConfirmarPedidoActionPerformed

    private void check30ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_check30ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_check30ActionPerformed

    private void check10ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_check10ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_check10ActionPerformed

    private void botonAceptarArticuloActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonAceptarArticuloActionPerformed

        boolean foco = this.txtCodigoArticulo.isFocusOwner();
        System.out.println("TIENE EL FOCO: " + foco);

        int descuento = 0;

        if (check10.isSelected()) {
            descuento = 10;
        } else if (check20.isSelected()) {
            descuento = 20;
        } else if (check30.isSelected()) {
            descuento = 30;
        } else if (check40.isSelected()) {
            descuento = 40;
        }

        if (comboBoxDetalleArticulo.getSelectedIndex() == -1 || !this.comboBoxDetalleArticulo.isEnabled()) {
            manA.traerArticulosComboBox(comboBoxDetalleArticulo);
            this.comboBoxDetalleArticulo.setSelectedIndex(-1);
            String codigo = this.txtCodigoArticulo.getText();
            System.out.println("CODIGO A BUSCAR: " + codigo);

            if (manA.existeID(codigo) && bandera) {
                manA.obtenerArticuloPorCodigo(codigo, m);
                mt.agregarArticuloVenta(tablaResumenVenta, modeloTablaResumenPedido, m, descuento);
                this.txtCodigoArticulo.setText("");
                this.txtCodigoArticulo.requestFocus();
                this.botonConfirmarPedido.setEnabled(true);
                this.comboBoxDetalleArticulo.removeAllItems();
                bandera = true;
                iB = 2;
                this.comboBoxDetalleArticulo.removeAllItems();
                manA.traerArticulosComboBox(comboBoxDetalleArticulo);
            } else {
                JOptionPane.showMessageDialog(null, "NO EXISTE ARTICULO CON DICHO CÓDIGO, INGRESE UNO VÁLIDO");
                //bandera = false;
                this.txtCodigoArticulo.setText("");

            }
        } else if (!comboBoxDetalleArticulo.getEditor().getItem().toString().equals("")) {
            String cadenaEscrita = comboBoxDetalleArticulo.getEditor().getItem().toString();
            m = manA.obtenerArticuloPorString(cadenaEscrita);
            mt.agregarArticuloVenta(tablaResumenVenta, modeloTablaResumenPedido, m, descuento);
            this.txtCodigoArticulo.setText("");
            this.txtCodigoArticulo.requestFocus();

        } else {
            JOptionPane.showMessageDialog(null, "INGRESE UN ARTICULO VALIDO");

        }

        group.clearSelection();
        this.txtCodigoArticulo.setText("");
        this.txtCodigoArticulo.setEnabled(true);
        this.comboBoxDetalleArticulo.setSelectedIndex(-1);
        this.comboBoxDetalleArticulo.setEnabled(true);
    }//GEN-LAST:event_botonAceptarArticuloActionPerformed

    private void txtCodigoArticuloKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtCodigoArticuloKeyReleased
        // TODO add your handling code here:

        if (this.txtCodigoArticulo.getText().equals("")) {
            this.comboBoxDetalleArticulo.setEnabled(true);
        } else {
            this.comboBoxDetalleArticulo.setEnabled(false);
        }

        int descuento = 0;

        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            //this.comboBoxDetalleArticulo.removeAll();

            manA.traerArticulosComboBox(comboBoxDetalleArticulo);
            this.comboBoxDetalleArticulo.setSelectedIndex(-1);
            String codigo = this.txtCodigoArticulo.getText();
            System.out.println("CODIGO A BUSCAR: " + codigo);
            if (manA.existeID(codigo) && bandera) {
                if (manA.verificarStock(codigo)) {
                    manA.obtenerArticuloPorCodigo(codigo, m);

                    if (this.check10.isSelected()) {
                        descuento = 10;
                    } else if (check20.isSelected()) {
                        descuento = 20;
                    } else if (check30.isSelected()) {
                        descuento = 30;
                    } else if (check40.isSelected()) {
                        descuento = 40;
                    }

                    mt.agregarArticuloVenta(tablaResumenVenta, modeloTablaResumenPedido, m, descuento);
                    this.txtCodigoArticulo.setText("");
                    this.txtCodigoArticulo.requestFocus();
                    //this.txtTotalAcumulado.setText(String.valueOf(Float.parseFloat(this.txtTotalAcumulado.getText()) + m.getPrecio_venta()));
                    //txtTotalRestante.setText(txtTotalAcumulado.getText());
                    this.botonConfirmarPedido.setEnabled(true);
                    //this.botonAgregarFormaPago.setEnabled(true);
                    this.comboBoxDetalleArticulo.removeAllItems();
                    bandera = true;
                    iB = 2;
                } else {
                    JOptionPane.showMessageDialog(null, "NO HAY STOCK PARA EL PRODUCTO SELECCIONADO");
                }

            } else {
                JOptionPane.showMessageDialog(null, "NO EXISTE ARTICULO CON DICHO CÓDIGO, INGRESE UNO VÁLIDO");
                //bandera = false;
                this.txtCodigoArticulo.setText("");

            }
            this.comboBoxDetalleArticulo.setEnabled(true);
            group.clearSelection();

        }
    }//GEN-LAST:event_txtCodigoArticuloKeyReleased

    private void comboBoxDetalleArticuloActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboBoxDetalleArticuloActionPerformed
        // TODO add your handling code here:

    }//GEN-LAST:event_comboBoxDetalleArticuloActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /*
         * Set the Nimbus look and feel
         */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /*
         * If Nimbus (introduced in Java SE 6) is not available, stay with the
         * default look and feel. For details see
         * http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(NuevaVenta.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(NuevaVenta.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(NuevaVenta.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(NuevaVenta.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /*
         * Create and display the form
         */
        java.awt.EventQueue.invokeLater(new Runnable() {

            public void run() {
                new NuevaVenta(null, null, null, null, null, null).setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton botonAceptarArticulo;
    private javax.swing.JButton botonAgregarFormaPago;
    private javax.swing.JButton botonConfirmarPedido;
    private javax.swing.JButton botonEditarCliente;
    private javax.swing.JButton botonEliminarFormaDePago;
    private javax.swing.JButton botonEliminarMercaderia;
    private javax.swing.JButton botonGuardarPedido;
    private javax.swing.JCheckBox check10;
    private javax.swing.JCheckBox check20;
    private javax.swing.JCheckBox check30;
    private javax.swing.JCheckBox check40;
    private javax.swing.JComboBox comboBoxDetalleArticulo;
    private javax.swing.JComboBox comboBoxNombreCliente;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JLabel labelCliente;
    private javax.swing.JLabel labelDNI;
    private javax.swing.JLabel labelDireccion;
    private javax.swing.JLabel labelTelefono;
    private javax.swing.JPanel panelCliente;
    private javax.swing.JPanel panelMercaderia;
    private javax.swing.JPanel panelResumenPedido;
    private javax.swing.JTable tablaFacturacion;
    private javax.swing.JTable tablaResumenVenta;
    private javax.swing.JTextField txtCodigoArticulo;
    private javax.swing.JTextField txtDeudaCtaCte;
    private javax.swing.JLabel txtTotalAcumulado;
    private javax.swing.JLabel txtTotalRestante;
    // End of variables declaration//GEN-END:variables
}
