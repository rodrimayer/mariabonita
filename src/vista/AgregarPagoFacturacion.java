/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista;

import controlador.manejadorCondVta;
import java.awt.event.KeyEvent;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import modelo.Cond_vta;
import modelo.Facturacion_boleta;
import vista.NuevaVenta.ModeloTablaFacturacion;

/**
 *
 * @author rmmayer
 */
public class AgregarPagoFacturacion extends javax.swing.JFrame {

    manejadorCondVta mcv = new manejadorCondVta();
    boolean b = true;
    Cond_vta cv = new Cond_vta();
    Facturacion_boleta dc = new Facturacion_boleta();
    DefaultTableModel modeloF = new DefaultTableModel();
    JTable tablaFacturacion = new JTable();
    float montoT;
    JFrame nv;

    /**
     * Creates new form AgregarPagoFacturacion
     *
     * @param tablaFacturacion
     */
    public AgregarPagoFacturacion(JTable tablaFacturacion, NuevaVenta.ModeloTablaFacturacion modelo, float montoTotal, JFrame NuevaVenta) {
        initComponents();
        modeloF = modelo;
        montoT = montoTotal;
        nv = NuevaVenta;
        this.tablaFacturacion = tablaFacturacion;
        mcv.cargarCondVta(JComboBoxDetallePago);
        this.txtCantCuotas.setEnabled(false);
        this.txtTotal.setText(String.valueOf(montoT));
        this.txtTotalRestante.setText(String.valueOf(montoT));

        //this.JComboBoxDetallePago.setSelectedIndex(-1);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel3 = new javax.swing.JLabel();
        jTextField2 = new javax.swing.JTextField();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txtCodigoPago = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        JComboBoxDetallePago = new javax.swing.JComboBox<>();
        jLabel4 = new javax.swing.JLabel();
        txtCantCuotas = new javax.swing.JTextField();
        botonAñadir = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();
        txtMonto = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        txtTotal = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        txtTotalRestante = new javax.swing.JLabel();

        jLabel3.setText("jLabel3");

        jTextField2.setText("jTextField2");

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("INSERTAR FORMA DE PAGO"));

        jLabel1.setText("Código de Pago:");

        jLabel2.setText("Detalle: ");

        JComboBoxDetallePago.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        JComboBoxDetallePago.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JComboBoxDetallePagoActionPerformed(evt);
            }
        });
        JComboBoxDetallePago.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                JComboBoxDetallePagoPropertyChange(evt);
            }
        });

        jLabel4.setText("Cant. Cuotas: ");

        botonAñadir.setText("AÑADIR");
        botonAñadir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonAñadirActionPerformed(evt);
            }
        });

        jLabel5.setText("Monto:");

        txtMonto.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtMontoKeyReleased(evt);
            }
        });

        jLabel6.setText("TOTAL A PAGAR:");

        txtTotal.setFont(new java.awt.Font("Tahoma", 1, 13)); // NOI18N

        jLabel7.setText("TOTAL RESTANTE:");

        txtTotalRestante.setFont(new java.awt.Font("Tahoma", 1, 13)); // NOI18N

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel4)
                                .addGap(0, 165, Short.MAX_VALUE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel1)
                                    .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 22, Short.MAX_VALUE)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txtMonto, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(txtCantCuotas, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(txtCodigoPago, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGap(18, 18, 18)
                                        .addComponent(jLabel2)))))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel6)
                        .addGap(18, 18, 18)
                        .addComponent(txtTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(58, 58, 58)))
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel7)
                        .addGap(18, 18, 18)
                        .addComponent(txtTotalRestante, javax.swing.GroupLayout.PREFERRED_SIZE, 66, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(JComboBoxDetallePago, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(botonAñadir, javax.swing.GroupLayout.DEFAULT_SIZE, 282, Short.MAX_VALUE))
                .addContainerGap())
        );

        jPanel1Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {txtCantCuotas, txtCodigoPago, txtMonto});

        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(txtCodigoPago, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2)
                    .addComponent(JComboBoxDetallePago, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(31, 31, 31)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel4)
                            .addComponent(txtCantCuotas, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(31, 31, 31)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel5)
                            .addComponent(txtMonto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGap(27, 27, 27)
                        .addComponent(botonAñadir, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 32, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(txtTotal, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel6))
                    .addComponent(txtTotalRestante, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel7, javax.swing.GroupLayout.Alignment.TRAILING)))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void JComboBoxDetallePagoPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_JComboBoxDetallePagoPropertyChange
        // TODO add your handling code here:

    }//GEN-LAST:event_JComboBoxDetallePagoPropertyChange

    private void JComboBoxDetallePagoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JComboBoxDetallePagoActionPerformed
        // TODO add your handling code here:
        if (!b && this.JComboBoxDetallePago.getSelectedIndex() != -1) {
            mcv.obtenerCond_cta(cv, this.JComboBoxDetallePago.getSelectedItem().toString());
            //System.out.println("PERMITE CU");
            if (cv.getPermite_cuotas() == 1) {
                this.txtCantCuotas.setEnabled(true);
                System.out.println("ENTRO POR EL TRUE");
            } else {
                this.txtCantCuotas.setEnabled(false);
                System.out.println("ENTRO POR EL FALSE");
            }

        } else {
            this.JComboBoxDetallePago.setSelectedIndex(-1);
        }
        b = false;


    }//GEN-LAST:event_JComboBoxDetallePagoActionPerformed

    private void botonAñadirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonAñadirActionPerformed
        // TODO add your handling code here:
        String busqueda = this.JComboBoxDetallePago.getSelectedItem().toString();
        mcv.obtenerCond_cta(cv, busqueda);
        Object[] detalle_caja = new Object[4];
        detalle_caja[0] = cv.getId_condvta();
        detalle_caja[1] = cv.getDesc_condvta();
        detalle_caja[2] = this.txtCantCuotas.getText();
        detalle_caja[3] = this.txtMonto.getText();

        tablaFacturacion.setModel(modeloF);
        modeloF.addRow(detalle_caja);

        float montoRestante = Float.parseFloat(this.txtTotalRestante.getText()) - Float.parseFloat(this.txtMonto.getText());

        this.txtTotalRestante.setText(String.valueOf(montoRestante));
        this.txtMonto.setText("");
        this.JComboBoxDetallePago.setSelectedIndex(-1);

        if (this.txtTotalRestante.getText().equals("0.0")) {
            JOptionPane.showMessageDialog(null, "EL PAGO TOTAL SE COMPLETO");
            this.dispose();
        }

        /*
        AgregarPagoFacturacion apg = new AgregarPagoFacturacion(tablaFacturacion, (ModeloTablaFacturacion) modeloF, montoT);
        apg.setLocationRelativeTo(null);
        apg.setVisible(true);
        this.dispose();*/
    }//GEN-LAST:event_botonAñadirActionPerformed

    private void txtMontoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtMontoKeyReleased
        // TODO add your handling code here:

        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            
            String busqueda = this.JComboBoxDetallePago.getSelectedItem().toString();
            mcv.obtenerCond_cta(cv, busqueda);
            Object[] detalle_caja = new Object[4];
            detalle_caja[0] = cv.getId_condvta();
            detalle_caja[1] = cv.getDesc_condvta();
            detalle_caja[2] = this.txtCantCuotas.getText();
            detalle_caja[3] = this.txtMonto.getText();

            tablaFacturacion.setModel(modeloF);
            modeloF.addRow(detalle_caja);

            float montoRestante = Float.parseFloat(this.txtTotalRestante.getText()) - Float.parseFloat(this.txtMonto.getText());

            this.txtTotalRestante.setText(String.valueOf(montoRestante));
            this.txtMonto.setText("");
            this.JComboBoxDetallePago.setSelectedIndex(-1);

            if (this.txtTotalRestante.getText().equals("0.0")) {
                JOptionPane.showMessageDialog(null, "EL PAGO TOTAL SE COMPLETO");
                this.dispose();
            }
        }

    }//GEN-LAST:event_txtMontoKeyReleased

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(AgregarPagoFacturacion.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(AgregarPagoFacturacion.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(AgregarPagoFacturacion.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(AgregarPagoFacturacion.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new AgregarPagoFacturacion(null, null, 0, null).setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox<String> JComboBoxDetallePago;
    private javax.swing.JButton botonAñadir;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JTextField jTextField2;
    private javax.swing.JTextField txtCantCuotas;
    private javax.swing.JTextField txtCodigoPago;
    private javax.swing.JTextField txtMonto;
    private javax.swing.JLabel txtTotal;
    private javax.swing.JLabel txtTotalRestante;
    // End of variables declaration//GEN-END:variables
}
