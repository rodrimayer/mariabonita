/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import conexion.conexionDB;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import modelo.Articulo;
import modelo.Talle;

/**
 *
 * @author Rodri Mayer
 */
public class manejadorArticulos {

    public class MiModelo extends DefaultTableModel {

        public boolean isCellEditable(int row, int column) {
            return false;
        }
    }

    public void traerTalles(JComboBox comboTalles) {

        comboTalles.removeAllItems();
        conexionDB c = new conexionDB();
        Connection con = c.conectar();

        String sql = "SELECT desc_talle from talle";
        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                comboTalles.addItem(rs1.getString(1));
            }
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(manejadorArticulos.class.getName()).log(Level.SEVERE, null, ex);
        }

        comboTalles.setSelectedIndex(-1);
    }

    public void traerTipos(JComboBox comboTipos) {
        comboTipos.removeAllItems();
        conexionDB c = new conexionDB();
        Connection con = c.conectar();

        String sql = "SELECT desc_tipo from tipo";
        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                comboTipos.addItem(rs1.getString(1));
            }
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(manejadorArticulos.class.getName()).log(Level.SEVERE, null, ex);
        }

        comboTipos.setSelectedIndex(-1);
    }

    public boolean existeID(String id) {
        boolean b = false;
        conexionDB c = new conexionDB();
        Connection con = c.conectar();
        String sql = "select * from articulo where codigo = '" + id + "'";
        System.out.println(sql);
        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                b = true;
            }
        } catch (SQLException ex) {
            Logger.getLogger(manejadorArticulos.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println("BANDERA: " + b);
        return b;
    }

    public int insertarArticulo(Articulo a) {
        int n = 0;
        conexionDB con = new conexionDB();
        Connection reg = con.conectar();
        String sql = "INSERT INTO articulo (codigo, desc_articulo, id_tipo, id_talle, precio_costo, precio_venta, stock) VALUES (?,?,?,?,?,?,?)";

        System.out.println("INSERT INTO mercaderia (id_mercaderia, nombre, id_tipo, orden, inactivo) "
                + "VALUES ('" + a.getCodigo() + "','" + a.getDesc_articulo() + "'," + a.getId_tipo() + "," + a.getId_talle() + "," + a.getPrecio_costo() + "," + a.getPrecio_venta() + "," + a.getStock() + ")");

        try {

            PreparedStatement ps = reg.prepareStatement(sql);
            ps.setString(1, a.getCodigo());
            ps.setString(2, a.getDesc_articulo());
            ps.setInt(3, a.getId_tipo());
            ps.setInt(4, a.getId_talle());
            ps.setFloat(5, a.getPrecio_costo());
            ps.setFloat(6, a.getPrecio_venta());
            ps.setInt(7, a.getStock());

            System.out.println(sql);

            n = ps.executeUpdate();
            reg.close();

        } catch (SQLException ex) {
            Logger.getLogger(manejadorArticulos.class.getName()).log(Level.SEVERE, null, ex);
        }

        return n;
    }

    

    public int encontrarIDTalle(String talle) {
        int id_talle = 0;

        String sql = "SELECT id_talle from TALLE where desc_talle = '" + talle + "'";
        System.out.println(sql);
        conexionDB con = new conexionDB();
        Connection reg = con.conectar();

        try {
            Statement st1 = reg.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                id_talle = rs1.getInt(1);
            }

            reg.close();

        } catch (Exception e) {
            System.err.println("ERROR TRAYENDO EL ID_TALLE: " + e);
        }

        System.out.println("ID_TALLE DESPUES DE LA CONSULTA: " + id_talle);
        return id_talle;
    }

    public int encontrarIDTipo(String tipo) {
        int id_tipo = 0;

        String sql = "SELECT id_tipo from TIPO where desc_tipo = '" + tipo + "'";
        conexionDB con = new conexionDB();
        Connection reg = con.conectar();

        try {
            Statement st1 = reg.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                id_tipo = rs1.getInt(1);
            }

            reg.close();

        } catch (Exception e) {
            System.err.println("ERROR TRAYENDO EL ID_TIPO: " + e);
        }

        return id_tipo;
    }

    public void traerArticulosComboBox(JComboBox comboBoxArticulos) {     
        

        String sql = "SELECT a.codigo, a.desc_articulo, t.desc_talle, a.precio_venta from Articulo a "
                + "JOIN talle t "
                + "on a.id_talle = t.id_talle "
                + "where a.stock <> 0";

        conexionDB c = new conexionDB();
        Connection con = c.conectar();

        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {                
                comboBoxArticulos.addItem(rs1.getString(2));                
            }
            
            comboBoxArticulos.setSelectedIndex(-1);

            con.close();

        } catch (Exception e) {
            System.err.println("ERROR TRAYENDO ARTICULOS: " + e);
        }
    }
    
    public Articulo obtenerArticuloPorString(String bus) {
        
        Talle t = new Talle();

        String sql = "SELECT a.codigo, a.desc_articulo, t.desc_talle, a.precio_venta, a.id_talle "
                + "FROM ARTICULO as a "
                + "JOIN TALLE as t "
                + "on a.id_talle = t.id_talle "
                + "where a.desc_articulo LIKE '%" + bus + "%'";

        String codigo = "";

        System.out.println(sql);

        conexionDB c = new conexionDB();
        Connection con = c.conectar();
        
        Articulo a = new Articulo();

        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                t.setId_talle(rs1.getInt(5));
                t.setDesc_talle(rs1.getString(3));
                a.setCodigo(rs1.getString(1));
                a.setDesc_articulo(rs1.getString(2));
                a.setId_talle(rs1.getInt(5));
                a.setTalle(t);
                a.setPrecio_venta(rs1.getFloat(4));
            }

        } catch (Exception e) {
            System.out.println("Error: " + e);
        }

        return a;

    }
    
    public void obtenerArticuloPorCodigo(String codigo, Articulo a) {
        
        Talle t = new Talle();

        String sql = "SELECT a.codigo, a.desc_articulo, t.desc_talle, a.precio_venta, a.id_talle "
                + "FROM ARTICULO as a "
                + "JOIN TALLE as t "
                + "on a.id_talle = t.id_talle "
                + "where a.codigo = '"+ codigo + "'";

        System.out.println(sql);

        conexionDB c = new conexionDB();
        Connection con = c.conectar();        

        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                t.setId_talle(rs1.getInt(5));
                t.setDesc_talle(rs1.getString(3));
                a.setCodigo(rs1.getString(1));
                a.setDesc_articulo(rs1.getString(2));
                a.setId_talle(rs1.getInt(5));
                a.setTalle(t);
                a.setPrecio_venta(rs1.getFloat(4));
            }

        } catch (Exception e) {
            System.out.println("Error: " + e);
        }

    }
    
    public String traerTalleID(int id_talle) {
        
        conexionDB c = new conexionDB();
        Connection con = c.conectar();
        String desc_talle = "";

        String sql = "SELECT desc_talle from talle where id_talle = "+id_talle;
        
        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                desc_talle = rs1.getString(1);
            }
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(manejadorArticulos.class.getName()).log(Level.SEVERE, null, ex);
        }

        return desc_talle;
    }
    
    public void actualizarStock(String codigo){
        
        conexionDB con = new conexionDB();
        Connection c = con.conectar();
        
        String sql = "UPDATE ARTICULO "
                + "SET stock = stock - 1 "
                + "WHERE codigo = '"+ codigo + "'";
        
        Statement st1;
        try {
            st1 = c.createStatement();
            st1.executeUpdate(sql);
            
            c.close();
            
        } catch (SQLException ex) {
            Logger.getLogger(manejadorArticulos.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
    }
    
    public boolean verificarStock (String codigo){
        boolean b = true;
        
        String sql = "SELECT stock "
                + "FROM Articulo "
                + "WHERE codigo = '"+ codigo + "'";
        
        System.out.println(sql);
        
        conexionDB c = new conexionDB();
        Connection con = c.conectar();
        
        Statement st1;
        ResultSet rs1;
        
        try {
            st1 = con.createStatement();
            rs1 = st1.executeQuery(sql);
            
            while(rs1.next()){
                if(rs1.getInt(1)==0){
                    b = false;
                }
            }
            
            con.close();
            
        } catch (SQLException ex) {
            Logger.getLogger(manejadorArticulos.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return b;
        
    }
   
}
