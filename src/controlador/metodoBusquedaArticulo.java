/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import conexion.conexionDB;
import vista.NuevaVenta;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JFrame;
import javax.swing.SwingConstants;

/**
 *
 * @author rodrimayer
 */
public class metodoBusquedaArticulo
{

    public NuevaVenta c ;
    //public JFrame cpd;
    
    public metodoBusquedaArticulo(NuevaVenta c) {
        this.c = c;
    }    
    
    
    public boolean comparar(String cadena) {
        Object[] lista = this.c.getcomboBoxMercaderia().getSelectedObjects();//       traer todos los items mejor.
        boolean encontrado = false;

        int xx = this.c.getcomboBoxMercaderia().getItemCount();

        for (int i = 0; i < xx; i++) {
            //  System.out.println("elemento seleccionado - "+lista[i]);
            if (cadena.equals(this.c.getcomboBoxMercaderia().getItemAt(i))) {
                //  nn=(String)boxNombre.getItemAt(i).toString(); 
                encontrado = true;
                break;
            }

        }
    return encontrado;

    }
    
     public DefaultComboBoxModel getLista(String cadenaEscrita, String campo) {
        DefaultComboBoxModel modelo = new DefaultComboBoxModel();
        
        try {
            conexionDB con = new conexionDB();
            Connection conex = con.conectar();
            Statement st1 = conex.createStatement();
            String sql = "select desc_articulo from ARTICULO WHERE " + campo + " LIKE '%" + cadenaEscrita + "%' and stock <> 0";          
            ResultSet res = st1.executeQuery(sql);
            System.out.println(sql);
            
            while (res.next()) {
                modelo.addElement(res.getString(1));
            }
            res.close();
            conex.close();
        } catch (SQLException ex) {

        }
        return modelo;
    }    
    
}
