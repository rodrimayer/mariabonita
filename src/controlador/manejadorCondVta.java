/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import conexion.conexionDB;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JComboBox;
import modelo.Cond_vta;

/**
 *
 * @author rmmayer
 */
public class manejadorCondVta {
    
    public void cargarCondVta (JComboBox detalleCondVta){
        conexionDB c = new conexionDB();
        Connection con = c.conectar();
        detalleCondVta.removeAllItems();
        
        String sql = "SELECT desc_condvta from COND_VTA";
        
        try{
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);
            
            while(rs1.next()){
                detalleCondVta.addItem(rs1.getString(1));
            }
            con.close();
            
        }catch (SQLException e){
            
        }
        
    }
    
    public Cond_vta obtenerCond_cta (Cond_vta cv, String busqueda){
        conexionDB c = new conexionDB();
        Connection con = c.conectar();        
        
        String sql = "SELECT id_condvta, desc_condvta, permite_cuotas from COND_VTA where desc_condvta = '"+ busqueda + "'";
        System.out.println(sql);
        try{
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);
            
            while(rs1.next()){
                cv.setId_condvta(rs1.getInt(1));
                cv.setDesc_condvta(busqueda);
                cv.setPermite_cuotas(rs1.getInt(3));
            }
            con.close();
            
        }catch (SQLException e){
            
        }
        
        
        return cv;
    }
    
    public void cargarCondVtaSinCuota (JComboBox detalleCondVta){
        conexionDB c = new conexionDB();
        Connection con = c.conectar();
        detalleCondVta.removeAllItems();
        
        String sql = "SELECT desc_condvta from COND_VTA where permite_cuotas = 0";
        
        try{
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);
            
            while(rs1.next()){
                detalleCondVta.addItem(rs1.getString(1));
            }
            con.close();
            
        }catch (SQLException e){
            
        }
        
    }
    
        
    
}
