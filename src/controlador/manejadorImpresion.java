/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import br.com.adilson.util.Extenso;
import br.com.adilson.util.PrinterMatrix;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import javax.print.Doc;
import javax.print.DocFlavor;
import javax.print.DocPrintJob;
import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import javax.print.SimpleDoc;
import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;
import modelo.Boleta;
import modelo.Cliente;
import modelo.Detalle_boleta;
import modelo.Usuario;

/**
 *
 * @author Rodri Mayer
 */
public class manejadorImpresion {
    
    manejadorFechas mf = new manejadorFechas();
    
    public void imprimirFactura(Boleta b, Cliente c, Usuario u, ArrayList<Detalle_boleta> aDB) {
        
        PrinterMatrix printer = new PrinterMatrix();

        Extenso e = new Extenso();

        e.setNumber(70.85);

        //Definir el tamanho del papel para la impresion  aca 25 lineas y 80 columnas
        printer.setOutSize(11+aDB.size(), 58);
        //Imprimir * de la 2da linea a 25 en la columna 1;
        // printer.printCharAtLin(2, 25, 1, "*");
        //Imprimir * 1ra linea de la columa de 1 a 80
        printer.printCharAtCol(1, 1, 32, "=");
        //Imprimir Encabezado nombre del La EMpresa
        printer.printTextWrap(1, 1, 1, 58, "MARIA BONITA ROPA Y ACCESORIOS");
        //printer.printTextWrap(linI, linE, colI, colE, null);
        printer.printTextWrap(2, 2, 1, 58, "Num. Boleta: 00000000"+b.getId_boleta());
        printer.printTextWrap(2, 2, 33, 80, "Fecha de Emision: "+mf.convertirFecha(b.getFecha_boleta()));
        printer.printTextWrap(3, 3, 1, 58, "Cliente: "+c.getNombre_cliente()+" "+c.getApellido_cliente());
        printer.printTextWrap(3, 3, 33, 80, "Vendedor: "+u.getNombre_usuario()+" "+u.getApellido_usuario());

        printer.printTextWrap(4, 4, 1, 58, "===============================");
        printer.printTextWrap(4, 4, 33, 80, "Art.       Desc.      P.Total");
        printer.printTextWrap(5, 5, 1, 58, "-------------------------------");
        
        int fila = 5;
        
        
        for (int i = 0; i < 3; i++) {
            printer.printTextWrap(fila, fila, 33, 150, aDB.get(i).getCodigo());
            printer.printTextWrap(fila, fila, 44, 150, aDB.get(i).getDescuento()+"%        $"+aDB.get(i).getPrecio());            
            fila++;
        }
        
        printer.printTextWrap(8, 8, 1, 58, "-------------------------------");
        printer.printTextWrap(8, 8, 33, 80, "TOTAL A PAGAR:          $"+b.getTotal());
        printer.printTextWrap(10, 10, 1, 58, "Esta boleta no tiene valor fiscal, solo para uso interno");
        
        

        
        printer.toFile("impresion.txt");

        FileInputStream inputStream = null;
        try {
            inputStream = new FileInputStream("impresion.txt");
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        }
        if (inputStream == null) {
            return;
        }

        DocFlavor docFormat = DocFlavor.INPUT_STREAM.AUTOSENSE;
        Doc document = new SimpleDoc(inputStream, docFormat, null);

        PrintRequestAttributeSet attributeSet = new HashPrintRequestAttributeSet();

        PrintService defaultPrintService = PrintServiceLookup.lookupDefaultPrintService();

        if (defaultPrintService != null) {
            DocPrintJob printJob = defaultPrintService.createPrintJob();
            try {
                printJob.print(document, attributeSet);

            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } else {
            System.err.println("No existen impresoras instaladas");
        }

        //inputStream.close();
    }
    
    public void imprimirDetalleFactura(Detalle_boleta db) {
        
        PrinterMatrix printer = new PrinterMatrix();

        Extenso e = new Extenso();

        e.setNumber(70.85);

        //Definir el tamanho del papel para la impresion  aca 25 lineas y 80 columnas
        printer.setOutSize(2, 58);
        
        printer.printTextWrap(1, 1, 1, 150, db.getCodigo());
        printer.printTextWrap(1, 1, 13, 150, db.getDescuento()+"%        $"+db.getPrecio());    
       

        
        printer.toFile("impresion.txt");

        FileInputStream inputStream = null;
        try {
            inputStream = new FileInputStream("impresion.txt");
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        }
        if (inputStream == null) {
            return;
        }

        DocFlavor docFormat = DocFlavor.INPUT_STREAM.AUTOSENSE;
        Doc document = new SimpleDoc(inputStream, docFormat, null);

        PrintRequestAttributeSet attributeSet = new HashPrintRequestAttributeSet();

        PrintService defaultPrintService = PrintServiceLookup.lookupDefaultPrintService();

        if (defaultPrintService != null) {
            DocPrintJob printJob = defaultPrintService.createPrintJob();
            try {
                printJob.print(document, attributeSet);

            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } else {
            System.err.println("No existen impresoras instaladas");
        }

        //inputStream.close();
    }
}
