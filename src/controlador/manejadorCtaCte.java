/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import conexion.conexionDB;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import modelo.Cliente;
import modelo.Cta_cte;
import modelo.Detalle_CtaCte;

/**
 *
 * @author rmmayer
 */
public class manejadorCtaCte {

    public void agregarMontoCuentaCorriente(Detalle_CtaCte dcc) {
        conexionDB con = new conexionDB();
        Connection reg = con.conectar();
        
        String sql = "INSERT INTO detalle_ctacte (id_ctacte, numero_cuota, vencimiento_cuota, valor_cuota, valor_cancelado, finalizado) VALUES (?,?,?,?,?,?)";
        System.out.println(sql);
        
        try{
            PreparedStatement ps = reg.prepareStatement(sql);
            ps.setInt(1, dcc.getId_ctacte());
            ps.setInt(2, dcc.getNumero_cuota());
            ps.setString(3, dcc.getVencimiento_cuota());
            ps.setFloat(4, dcc.getValor_cuota());
            ps.setFloat(5, 0);
            ps.setInt(6, 0);
            
            ps.executeUpdate();
            
            reg.close();
            
        }catch(SQLException e){
            System.out.println("ERROR EN EL AGREGADO DE DETALLE DE CUENTA CORRIENTE");
        }
    }

    public void crearCuentaCorriente(int id_cliente) {
        conexionDB con = new conexionDB();
        Connection c = con.conectar();

        String sql = "INSERT INTO CTA_CTE (id_cliente) VALUES (?) ";

        PreparedStatement ps;

        try {
            ps = c.prepareStatement(sql);
            ps.setInt(1, id_cliente);

            ps.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(manejadorCtaCte.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public boolean existeCtaCte(int id) {
        boolean b = false;
        conexionDB c = new conexionDB();
        Connection con = c.conectar();
        String sql = "select * from cta_cte where id_cliente = " + id;
        System.out.println(sql);
        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                b = true;
            }
        } catch (SQLException ex) {
            Logger.getLogger(manejadorArticulos.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println("BANDERA: " + b);
        return b;
    }

    public Cta_cte TraerCtaCte(int id) {
        boolean b = false;
        conexionDB c = new conexionDB();
        Connection con = c.conectar();
        Cta_cte cc = new Cta_cte();

        String sql = "select id_ctacte, id_cliente from cta_cte where id_cliente = " + id;

        System.out.println(sql);
        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                b = true;
                cc.setCta_cte(rs1.getInt(1));
                cc.setId_cliente(rs1.getInt(2));
            }
        } catch (SQLException ex) {
            Logger.getLogger(manejadorArticulos.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println("BANDERA: " + b);

        return cc;
    }

    public float deudaCtaCte(int id_ctacte) {
        float deudaCtaCte = 0;

        conexionDB c = new conexionDB();
        Connection con = c.conectar();

        String sql = "SELECT id_ctacte, (SUM(valor_cuota)-SUM(valor_cancelado)) "
                + "FROM detalle_ctacte "
                + "WHERE id_ctacte = "+id_ctacte+" "
                + "GROUP BY id_ctacte";
        
        System.out.println(sql);
        
        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                deudaCtaCte = rs1.getFloat(2);
            }
            
            con.close();
            
        } catch (SQLException ex) {
            Logger.getLogger(manejadorArticulos.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return deudaCtaCte;
    }
    
    public void pagoCuota (int id_detCtaCte){
        conexionDB c = new conexionDB();
        Connection con = c.conectar();

        String sql = "UPDATE detalle_ctacte set valor_cancelado = valor_cuota "
                + "WHERE id_detallectacte = "+id_detCtaCte;
        
        System.out.println(sql);
        
        try {
            Statement st1 = con.createStatement();
            st1.executeUpdate(sql);
            
            con.close();
            
        } catch (SQLException ex) {
            Logger.getLogger(manejadorArticulos.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void pagoCuotaParcial (int id_detCtaCte, float monto){
        conexionDB c = new conexionDB();
        Connection con = c.conectar();

        String sql = "UPDATE detalle_ctacte set valor_cancelado = "+monto
                + " WHERE id_detallectacte = "+id_detCtaCte;
        
        System.out.println(sql);
        
        try {
            Statement st1 = con.createStatement();
            st1.executeUpdate(sql);
            
            con.close();
            
        } catch (SQLException ex) {
            Logger.getLogger(manejadorArticulos.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    

}
