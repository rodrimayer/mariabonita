/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import conexion.conexionDB;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author rmmayer
 */
public class manejadorFechas {
    
    public String obtenerFechaActual() {

        Calendar fecha = Calendar.getInstance();
        String dia, mes, annio;
        String fechaSql = null;
        
        Date date = new Date();
        //Caso 1: obtener la hora y salida por pantalla con formato:
        DateFormat hourFormat = new SimpleDateFormat("HH:mm:ss");
        System.out.println("Hora: " + hourFormat.format(date));
        System.out.println("HORA: " + Integer.parseInt(hourFormat.format(date).toString().substring(0, 2)));

        boolean mayor19;

        if (Integer.parseInt(hourFormat.format(date).toString().substring(0, 2)) >= 0 && Integer.parseInt(hourFormat.format(date).toString().substring(0, 2)) < 19) {
            mayor19 = false;
        } else {
            mayor19 = true;
        }

        if (mayor19) {
            System.out.println("MAYOR A 19");
            fecha.add(Calendar.DAY_OF_YEAR,1);
            
        } else {
            System.out.println("MENOR A 19");            
        }
        
        if ((fecha.get(Calendar.DATE)) < 10) {
                dia = "0" + Integer.toString(fecha.get(Calendar.DATE));
            } else {
                dia = Integer.toString(fecha.get(Calendar.DATE));

            }

            if (fecha.get(Calendar.MONTH) + 1 < 10) {
                mes = "0" + Integer.toString(fecha.get(Calendar.MONTH) + 1);

            } else {
                mes = Integer.toString(fecha.get(Calendar.MONTH) + 1);

            }

            annio = Integer.toString(fecha.get(Calendar.YEAR));

            fechaSql = annio + mes + dia;
            System.out.println("FECHA SQL: " + fechaSql);



        return fechaSql;

    }
    
    public String obtenerFechasVtoCtaCte (String fechaActual, int nroCuota){
        String fechaVto = "";
        Calendar fecha = Calendar.getInstance();
        String dia = this.obtenerDiaVto();
        String mes = "";
        fecha.add(Calendar.MONTH, nroCuota);
        String annio = String.valueOf(fecha.get(Calendar.YEAR));
        
        if((fecha.get(Calendar.MONTH) + 1) < 10){
            mes = "0" + String.valueOf(fecha.get(Calendar.MONTH) + 1);
        }else{
            mes = String.valueOf(fecha.get(Calendar.MONTH) + 1);
        }
        
        
        //mes = fechaActual.substring(2, 4);
        
        System.out.println("mes de la cuota: " + mes);
        System.out.println("año de la cuota: " + annio);
        fechaVto = annio + mes + dia;
        System.out.println("fecha vto: "+fechaVto);
        
        
        return fechaVto;
        
    }
    
    public String obtenerDiaVto (){
        String dia = "";
        conexionDB con = new conexionDB();
        Connection reg = con.conectar();
        
        String sql = "SELECT valor "
                + "FROM configuracion "
                + "WHERE desc_config = 'dia_vto_cuotas'";
        
        System.out.println("sql para traer dia vto: " + sql);
        
        try{
            Statement st = reg.createStatement();
            ResultSet rs = st.executeQuery(sql);
            
            while(rs.next()){
               dia = String.valueOf(rs.getInt(1));                     
            }
            
            reg.close();
            
        }catch(SQLException e){
            System.out.println("ERROR AL TRAER DIA DE VTO EN CONFIGURACIONES");
        }
        
        
        return dia;
    }
    
    public String convertirFecha(String fecha){
        String fechaConv = fecha.substring(6,8)+"/"+fecha.substring(4, 6)+"/"+fecha.substring(0,4);
        System.out.println("fecha convertida: "+fechaConv);
        return fechaConv;
    }
    
}
