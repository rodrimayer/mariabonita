/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import conexion.conexionDB;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import modelo.Articulo;
import modelo.Boleta;
import vista.AgregarPagoCtaCte.ModeloTablaFacturacionCuotas;
import vista.GestionCtaCte;
import vista.Inicio.ModeloTablas;
import vista.NuevaVenta;
import vista.NuevaVenta.ModeloTablaArticulosVenta;
import vista.NuevaVenta.ModeloTablaFacturacion;

/**
 *
 * @author rmmayer
 */
public class manejadorTablas {

    public ModeloTablaArticulosVenta diseñoTablaArticulosVenta(JTable tablaArticulos, ModeloTablaArticulosVenta modelo) {
        JTable tA = tablaArticulos;
        tablaArticulos.setModel(modelo);
        DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
        centerRenderer.setHorizontalAlignment(JLabel.CENTER);
        DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
        rightRenderer.setHorizontalAlignment(JLabel.RIGHT);

        tA.setModel(modelo);
        modelo.addColumn("CODIGO");
        modelo.addColumn("ARTICULO");
        modelo.addColumn("TALLE");
        modelo.addColumn("PRECIO");
        modelo.addColumn("DESCUENTO(%)");
        modelo.addColumn("PRECIO C/DESC");

        tA.setSize(947, 110);

        tA.getColumnModel().getColumn(0).setMaxWidth(100);
        tA.getColumnModel().getColumn(0).setMinWidth(100);
        tA.getColumnModel().getColumn(0).setPreferredWidth(100);
        tA.getColumnModel().getColumn(0).setCellRenderer(centerRenderer);

        tA.getColumnModel().getColumn(1).setCellRenderer(centerRenderer);

        tA.getColumnModel().getColumn(2).setMaxWidth(100);
        tA.getColumnModel().getColumn(2).setMinWidth(100);
        tA.getColumnModel().getColumn(2).setPreferredWidth(100);
        tA.getColumnModel().getColumn(2).setCellRenderer(centerRenderer);

        tA.getColumnModel().getColumn(3).setMaxWidth(100);
        tA.getColumnModel().getColumn(3).setMinWidth(100);
        tA.getColumnModel().getColumn(3).setPreferredWidth(100);
        tA.getColumnModel().getColumn(3).setCellRenderer(centerRenderer);

        tA.getColumnModel().getColumn(4).setMaxWidth(100);
        tA.getColumnModel().getColumn(4).setMinWidth(100);
        tA.getColumnModel().getColumn(4).setPreferredWidth(100);
        tA.getColumnModel().getColumn(4).setCellRenderer(centerRenderer);

        tA.getColumnModel().getColumn(5).setMaxWidth(120);
        tA.getColumnModel().getColumn(5).setMinWidth(120);
        tA.getColumnModel().getColumn(5).setPreferredWidth(120);
        tA.getColumnModel().getColumn(5).setCellRenderer(centerRenderer);

        return modelo;

    }

    public ModeloTablaArticulosVenta agregarArticuloVenta(JTable tablaArticulos, ModeloTablaArticulosVenta modelo, Articulo m, int descuento) {
        Object[] art = new Object[6];
        tablaArticulos.setModel(modelo);

        art[0] = m.getCodigo();
        art[1] = m.getDesc_articulo();
        //art[2] = m.getId_talle();
        art[2] = m.getTalle().getDesc_talle();
        art[3] = m.getPrecio_venta();
        if (descuento != 0) {
            art[4] = String.valueOf(descuento);
            art[5] = (m.getPrecio_venta() * (100 - descuento) / 100);
        } else {
            art[4] = "-";
            art[5] = m.getPrecio_venta();
        }

        modelo.addRow(art);

        return modelo;
    }

    public void traerArticulos(JTable tablaArticulos, DefaultTableModel modelo) {
        DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
        centerRenderer.setHorizontalAlignment(JLabel.CENTER);
        DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
        rightRenderer.setHorizontalAlignment(JLabel.RIGHT);

        tablaArticulos.setModel(modelo);
        modelo.addColumn("CODIGO");
        modelo.addColumn("ARTICULO");
        modelo.addColumn("TALLE");
        modelo.addColumn("PRECIO");

        tablaArticulos.getColumnModel().getColumn(0).setMaxWidth(100);
        tablaArticulos.getColumnModel().getColumn(0).setMinWidth(100);
        tablaArticulos.getColumnModel().getColumn(0).setPreferredWidth(100);
        tablaArticulos.getColumnModel().getColumn(0).setCellRenderer(centerRenderer);

        tablaArticulos.getColumnModel().getColumn(1).setCellRenderer(centerRenderer);

        tablaArticulos.getColumnModel().getColumn(2).setMaxWidth(50);
        tablaArticulos.getColumnModel().getColumn(2).setMinWidth(50);
        tablaArticulos.getColumnModel().getColumn(2).setPreferredWidth(50);
        tablaArticulos.getColumnModel().getColumn(2).setCellRenderer(centerRenderer);

        tablaArticulos.getColumnModel().getColumn(3).setMaxWidth(75);
        tablaArticulos.getColumnModel().getColumn(3).setMinWidth(75);
        tablaArticulos.getColumnModel().getColumn(3).setPreferredWidth(75);
        tablaArticulos.getColumnModel().getColumn(3).setCellRenderer(rightRenderer);

        Object[] articulo = new Object[4];

        String sql = "SELECT a.codigo, a.desc_articulo, t.desc_talle, a.precio_venta from Articulo a "
                + "JOIN talle t "
                + "on a.id_talle = t.id_talle "
                + "where a.stock <> 0";

        conexionDB c = new conexionDB();
        Connection con = c.conectar();

        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                articulo[0] = rs1.getString(1);
                articulo[1] = rs1.getString(2);
                articulo[2] = rs1.getString(3);
                articulo[3] = rs1.getFloat(4);

                modelo.addRow(articulo);
            }

            con.close();

        } catch (Exception e) {
            System.err.println("ERROR TRAYENDO ARTICULOS: " + e);
        }
    }

    public ModeloTablaFacturacion diseñoTablaFacturacion(JTable tablaFacturacion, ModeloTablaFacturacion modelo) {
        JTable tA = tablaFacturacion;
        tablaFacturacion.setModel(modelo);
        DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
        centerRenderer.setHorizontalAlignment(JLabel.CENTER);
        DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
        rightRenderer.setHorizontalAlignment(JLabel.RIGHT);

        tA.setModel(modelo);
        modelo.addColumn("ID_PAGO");
        modelo.addColumn("DETALLE");
        modelo.addColumn("CUOTAS");
        modelo.addColumn("MONTO");

        tA.getColumnModel().getColumn(0).setMaxWidth(100);
        tA.getColumnModel().getColumn(0).setMinWidth(100);
        tA.getColumnModel().getColumn(0).setPreferredWidth(100);
        tA.getColumnModel().getColumn(0).setCellRenderer(centerRenderer);

        tA.getColumnModel().getColumn(1).setCellRenderer(centerRenderer);

        tA.getColumnModel().getColumn(2).setMaxWidth(100);
        tA.getColumnModel().getColumn(2).setMinWidth(100);
        tA.getColumnModel().getColumn(2).setPreferredWidth(100);
        tA.getColumnModel().getColumn(2).setCellRenderer(centerRenderer);

        tA.getColumnModel().getColumn(3).setMaxWidth(200);
        tA.getColumnModel().getColumn(3).setMinWidth(200);
        tA.getColumnModel().getColumn(3).setPreferredWidth(200);
        tA.getColumnModel().getColumn(3).setCellRenderer(rightRenderer);

        return modelo;
    }

    public ModeloTablaFacturacionCuotas diseñoTablaFacturacionCuotas(JTable tablaFacturacion, ModeloTablaFacturacionCuotas modelo) {
        JTable tA = tablaFacturacion;
        tablaFacturacion.setModel(modelo);
        DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
        centerRenderer.setHorizontalAlignment(JLabel.CENTER);
        DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
        rightRenderer.setHorizontalAlignment(JLabel.RIGHT);

        tA.setModel(modelo);
        modelo.addColumn("ID_PAGO");
        modelo.addColumn("DETALLE");
        modelo.addColumn("CUOTAS");
        modelo.addColumn("MONTO");

        tA.getColumnModel().getColumn(0).setMaxWidth(100);
        tA.getColumnModel().getColumn(0).setMinWidth(100);
        tA.getColumnModel().getColumn(0).setPreferredWidth(100);
        tA.getColumnModel().getColumn(0).setCellRenderer(centerRenderer);

        tA.getColumnModel().getColumn(1).setCellRenderer(centerRenderer);

        tA.getColumnModel().getColumn(2).setMaxWidth(75);
        tA.getColumnModel().getColumn(2).setMinWidth(75);
        tA.getColumnModel().getColumn(2).setPreferredWidth(75);
        tA.getColumnModel().getColumn(2).setCellRenderer(centerRenderer);

        tA.getColumnModel().getColumn(3).setMaxWidth(75);
        tA.getColumnModel().getColumn(3).setMinWidth(75);
        tA.getColumnModel().getColumn(3).setPreferredWidth(75);
        tA.getColumnModel().getColumn(3).setCellRenderer(rightRenderer);

        return modelo;
    }

    public ModeloTablas diseñoTablaVentasDelDia(JTable tablaVentasDelDia, ModeloTablas modelo) {
        JTable tV = tablaVentasDelDia;
        tablaVentasDelDia.setModel(modelo);
        DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
        centerRenderer.setHorizontalAlignment(JLabel.CENTER);
        DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
        rightRenderer.setHorizontalAlignment(JLabel.RIGHT);

        tV.setModel(modelo);
        modelo.addColumn("N°");
        modelo.addColumn("CLIENTE");
        modelo.addColumn("MONTO");

        tV.getColumnModel().getColumn(0).setCellRenderer(rightRenderer);
        tV.getColumnModel().getColumn(1).setCellRenderer(rightRenderer);
        tV.getColumnModel().getColumn(2).setCellRenderer(rightRenderer);

        return modelo;
    }

    public ModeloTablas agregarVentasDelDia(JTable tablaVentasDelDia, ModeloTablas modelo, Boleta b) {
        Object[] art = new Object[3];
        tablaVentasDelDia.setModel(modelo);
        DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
        centerRenderer.setHorizontalAlignment(JLabel.CENTER);

        tablaVentasDelDia.getColumnModel().getColumn(0).setCellRenderer(centerRenderer);
        tablaVentasDelDia.getColumnModel().getColumn(1).setCellRenderer(centerRenderer);
        tablaVentasDelDia.getColumnModel().getColumn(2).setCellRenderer(centerRenderer);

        art[0] = b.getId_boleta();
        art[1] = String.valueOf(b.getCliente().getApellido_cliente()) + " " + String.valueOf(b.getCliente().getNombre_cliente());
        art[2] = "$ " + String.valueOf(b.getTotal());

        modelo.addRow(art);

        return modelo;
    }

    public void traerVentasDelDia(JTable tablaVentasDelDia, ModeloTablas modelo) {
        Object[] art = new Object[3];
        tablaVentasDelDia.setModel(modelo);
        DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
        centerRenderer.setHorizontalAlignment(JLabel.CENTER);

        tablaVentasDelDia.getColumnModel().getColumn(0).setCellRenderer(centerRenderer);
        tablaVentasDelDia.getColumnModel().getColumn(1).setCellRenderer(centerRenderer);
        tablaVentasDelDia.getColumnModel().getColumn(2).setCellRenderer(centerRenderer);

        conexionDB con = new conexionDB();
        Connection c = con.conectar();

        String sql = "SELECT b.id_boleta, b.fecha_boleta, b.id_cliente, b.total, c.nombre_cliente, c.apellido_cliente "
                + "FROM boleta b "
                + "JOIN cliente c "
                + "ON b.id_cliente = c.id_cliente "
                + "WHERE cerrada = 0 "
                + "ORDER BY b.id_boleta asc";

        Statement st1;
        ResultSet rs1;

        try {
            st1 = c.createStatement();
            rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                art[0] = rs1.getInt(1);
                art[1] = String.valueOf(rs1.getString(6)) + " " + String.valueOf(rs1.getString(5));
                art[2] = "$ " + String.valueOf(rs1.getFloat(4));

                modelo.addRow(art);
            }
        } catch (SQLException ex) {
            Logger.getLogger(manejadorTablas.class.getName()).log(Level.SEVERE, null, ex);
        }

        //return modelo;
    }

    public ModeloTablas diseñoTablaDeudores(JTable tablaDeudores, ModeloTablas modelo) {
        JTable tV = tablaDeudores;
        tablaDeudores.setModel(modelo);
        DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
        centerRenderer.setHorizontalAlignment(JLabel.CENTER);
        DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
        rightRenderer.setHorizontalAlignment(JLabel.RIGHT);

        tV.setModel(modelo);
        modelo.addColumn("ID_CLIENTE");
        modelo.addColumn("ID_CTACTE");
        modelo.addColumn("CLIENTE");
        modelo.addColumn("TELEFONO");
        modelo.addColumn("DEUDA ACUMULADA");

        tV.getColumnModel().getColumn(0).setMaxWidth(0);
        tV.getColumnModel().getColumn(0).setMinWidth(0);
        tV.getColumnModel().getColumn(0).setPreferredWidth(0);

        tV.getColumnModel().getColumn(1).setMaxWidth(0);
        tV.getColumnModel().getColumn(1).setMinWidth(0);
        tV.getColumnModel().getColumn(1).setPreferredWidth(0);

        tV.getColumnModel().getColumn(2).setCellRenderer(rightRenderer);
        tV.getColumnModel().getColumn(3).setCellRenderer(rightRenderer);
        tV.getColumnModel().getColumn(4).setCellRenderer(rightRenderer);

        return modelo;
    }

    public void traerDeudores(JTable tablaDeudores, ModeloTablas modelo) {
        Object[] art = new Object[5];
        //tablaDeudores.removeAll();
        tablaDeudores.setModel(modelo);
        DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
        centerRenderer.setHorizontalAlignment(JLabel.CENTER);

        tablaDeudores.getColumnModel().getColumn(2).setCellRenderer(centerRenderer);
        tablaDeudores.getColumnModel().getColumn(3).setCellRenderer(centerRenderer);
        tablaDeudores.getColumnModel().getColumn(4).setCellRenderer(centerRenderer);

        conexionDB con = new conexionDB();
        Connection c = con.conectar();

        String sql = "SELECT c.id_cliente, cc.id_ctacte, c.nombre_cliente, c.apellido_cliente, c.telefono, sum(dcc.valor_cuota) as deuda, sum(dcc.valor_cancelado) as pagado "
                + "FROM detalle_ctacte as dcc "
                + "JOIN cta_cte as cc "
                + "ON cc.id_ctacte = dcc.id_ctacte "
                + "JOIN cliente as c "
                + "ON cc.id_cliente = c.id_cliente "
                + "GROUP BY cc.id_ctacte, c.nombre_cliente, c.apellido_cliente "
                + "ORDER BY (sum(dcc.valor_cuota) - sum(dcc.valor_cancelado)) desc";

        Statement st1;
        ResultSet rs1;

        try {
            st1 = c.createStatement();
            rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                if (rs1.getFloat(6) - rs1.getFloat(7) != 0) {
                    art[0] = rs1.getInt(1);
                    art[1] = rs1.getInt(2);
                    art[2] = String.valueOf(rs1.getString(3)) + " " + String.valueOf(rs1.getString(4));
                    art[3] = rs1.getString(5);
                    art[4] = "$ " + String.valueOf(rs1.getFloat(6) - rs1.getFloat(7));

                    modelo.addRow(art);
                }

            }
        } catch (SQLException ex) {
            Logger.getLogger(manejadorTablas.class.getName()).log(Level.SEVERE, null, ex);
        }

        //return modelo;
    }

    public void diseñoTablasCuotasCtaCte(JTable tablaDeudores, GestionCtaCte.ModeloTablaCuotasCtaCte modelo) {
        tablaDeudores.setModel(modelo);
        DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
        centerRenderer.setHorizontalAlignment(JLabel.CENTER);

        modelo.addColumn("ID");
        modelo.addColumn("Nº CUOTA");
        modelo.addColumn("MONTO");
        modelo.addColumn("CANCELADO");
        modelo.addColumn("VTO");

        tablaDeudores.getColumnModel().getColumn(0).setMaxWidth(0);
        tablaDeudores.getColumnModel().getColumn(0).setMinWidth(0);
        tablaDeudores.getColumnModel().getColumn(0).setPreferredWidth(0);

        tablaDeudores.getColumnModel().getColumn(1).setCellRenderer(centerRenderer);
        tablaDeudores.getColumnModel().getColumn(2).setCellRenderer(centerRenderer);
        tablaDeudores.getColumnModel().getColumn(3).setCellRenderer(centerRenderer);
        tablaDeudores.getColumnModel().getColumn(4).setCellRenderer(centerRenderer);

    }

    public void traerCuotasCtaCte(JTable tablaDeudores, GestionCtaCte.ModeloTablaCuotasCtaCte modelo, int id_ctacte) {
        Object[] art = new Object[5];
        conexionDB con = new conexionDB();
        Connection c = con.conectar();

        String sql = "SELECT id_detallectacte, numero_cuota, valor_cuota, valor_cancelado, vencimiento_cuota "
                + "FROM detalle_ctacte "
                + "WHERE id_ctacte = " + id_ctacte + " and valor_cuota <> valor_cancelado";

        Statement st1;
        ResultSet rs1;

        try {
            st1 = c.createStatement();
            rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                art[0] = rs1.getInt(1);
                art[1] = rs1.getInt(2);
                art[2] = rs1.getFloat(3);
                art[3] = rs1.getFloat(4);
                art[4] = rs1.getString(5);

                modelo.addRow(art);
            }
        } catch (SQLException ex) {
            Logger.getLogger(manejadorTablas.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
