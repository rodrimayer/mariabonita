/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import conexion.conexionDB;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import modelo.Boleta;
import modelo.Cliente;
import modelo.Detalle_boleta;
import modelo.Facturacion_boleta;
import modelo.Usuario;

/**
 *
 * @author rmmayer
 */
public class manejadorFacturacion {
    
    public void insertarBoleta(Boleta b, Usuario u){
        
        conexionDB con = new conexionDB();
        Connection reg = con.conectar();
        
        System.out.println("id_cliente: "+b.getCliente().getId_cliente() + " fecha_boleta: "+ b.getFecha_boleta());
        
        String sql = "INSERT INTO boleta (id_cliente, fecha_boleta, total, cerrada, vendedor) VALUES (?,?,?,?,?)";        
        System.out.println(sql);
        
        try{
            PreparedStatement ps = reg.prepareStatement(sql);
            ps.setInt(1, b.getCliente().getId_cliente());
            ps.setString(2, b.getFecha_boleta());
            ps.setFloat(3, b.getTotal());
            ps.setInt(4, 0);
            ps.setInt(5, u.getId_usuario());
            
            ps.executeUpdate();
            
            reg.close();
            
        }catch(SQLException e){
            System.out.println("ERROR EN EL AGREGADO DE HEADER DE BOLETA");
        }
    }
    
    public void insertarDetallesBoleta(int id_boleta, Detalle_boleta db){
        
        conexionDB con = new conexionDB();
        Connection reg = con.conectar();
        
        String sql = "INSERT INTO detalle_boleta (id_boleta, codigo, descuento, precio_total) VALUES (?,?,?,?)";
        System.out.println(sql);
        
        try{
            PreparedStatement ps = reg.prepareStatement(sql);
            ps.setInt(1, id_boleta);
            ps.setString(2, db.getCodigo());
            ps.setInt(3, db.getDescuento());
            ps.setFloat(4, db.getPrecio());
            
            ps.executeUpdate();
            
            reg.close();
            
        }catch(SQLException e){
            System.out.println("ERROR EN EL AGREGADO DE DETALLE DE BOLETA");
        }
        
    }
    
    public void insertarFormasDePago(int id_boleta, Facturacion_boleta dc){
        conexionDB con = new conexionDB();
        Connection reg = con.conectar();
        
        String sql = "INSERT INTO facturacion_boleta (id_boleta, id_condvta, cant_cuotas, monto) VALUES (?,?,?,?)";
        System.out.println(sql);
        
        try{
            PreparedStatement ps = reg.prepareStatement(sql);
            ps.setInt(1, id_boleta);
            ps.setInt(2, dc.getId_condvta());
            ps.setInt(3, dc.getCant_cuotas());
            ps.setFloat(4, dc.getTotal());
            
            ps.executeUpdate();
            
            reg.close();
            
        }catch(SQLException e){
            System.out.println("ERROR EN EL AGREGADO DE DETALLE DE BOLETA");
        }
    }
    
    public int obtenerID_boleta(){
        int ultBoleta = 0;
        
        conexionDB con = new conexionDB();
        Connection reg = con.conectar();
        
        String sql = "SELECT id_boleta from boleta order by id_boleta desc limit 1";
        System.out.println(sql);
        
        try{
            Statement st = reg.createStatement();
            ResultSet rs = st.executeQuery(sql);
            
            while(rs.next()){
                ultBoleta = rs.getInt(1);
            }
            
            reg.close();
            
        }catch(SQLException e){
            System.out.println("ERROR AL TRAER ULTIMA BOLETA");
        }
        
        return ultBoleta;
    }
    
}
