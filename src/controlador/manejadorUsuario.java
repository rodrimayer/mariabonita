/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import conexion.conexionDB;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import modelo.Privilegio;
import modelo.Usuario;

/**
 *
 * @author rmmayer
 */
public class manejadorUsuario {

    public Usuario traerUsuario(String cuenta, String contraseña) {
        Usuario u = new Usuario();
        Privilegio p = new Privilegio();
        conexionDB c = new conexionDB();
        Connection con = c.conectar();        

        String sql = "SELECT u.id_usuario, u.cuenta_usuario, u.contraseña_usuario, u.nombre_usuario, u.apellido_usuario, p.id_privilegio, p.desc_privilegio "
                + "FROM usuario u "
                + "JOIN privilegio p "
                + "ON u.id_privilegio = p.id_privilegio "
                + "WHERE u.cuenta_usuario = '" + cuenta + "' and u.contraseña_usuario = '" + contraseña + "'";

        Statement st1;
        try {
            st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                p.setId_privilegio(rs1.getInt(6));
                p.setDesc_privilegio(rs1.getString(7));
                u.setId_usuario(rs1.getInt(1));
                u.setCuenta_usuario(rs1.getString(2));
                u.setContraseña_usuario(rs1.getString(3));
                u.setNombre_usuario(rs1.getString(4));
                u.setApellido_usuario(rs1.getString(5));
                u.setPrivilegio(p);

                System.out.println("nombre usuario: "+u.getNombre_usuario());                
                System.out.println("apellido usuario: "+u.getApellido_usuario());                
            }            
            
        } catch (SQLException ex) {
            Logger.getLogger(manejadorUsuario.class.getName()).log(Level.SEVERE, null, ex);
        }

        return u;
    }
    
    public boolean existeUsuario (String cuenta, String contraseña){
        boolean b = false;
        conexionDB c = new conexionDB();
        Connection con = c.conectar();
        String sql = "SELECT u.id_usuario, u.cuenta_usuario, u.contraseña_usuario, u.nombre_usuario, u.apellido_usuario, p.id_privilegio, p.desc_privilegio "
                + "FROM usuario u "
                + "JOIN privilegio p "
                + "ON u.id_privilegio = p.id_privilegio "
                + "WHERE u.cuenta_usuario = '" + cuenta + "' and u.contraseña_usuario = '" + contraseña + "'";

        Statement st1;
        try {
            st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {                
                b = true;
            }            
            
        } catch (SQLException ex) {
            Logger.getLogger(manejadorUsuario.class.getName()).log(Level.SEVERE, null, ex);
        }               
        
        return b;
    }

}
