/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import conexion.conexionDB;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import modelo.Boleta;
import modelo.Cliente;
import modelo.Cta_cte;
import modelo.Detalle_boleta;

/**
 *
 * @author rmmayer
 */
public class manejadorCliente {
    
    public class MiModelo extends DefaultTableModel {

        public boolean isCellEditable(int row, int column) {
            return false;
        }
    }
    
    public void agregarCliente(Cliente ct){
        String sqlAgregarCliente = "INSERT INTO cliente (id_cliente, nombre_cliente, apellido_cliente, direccion, localidad, telefono, mail) VALUES (?,?,?,?,?,?,?)";
        conexionDB c = new conexionDB();
        Connection con = c.conectar();
        try {
            PreparedStatement ps1 = con.prepareStatement(sqlAgregarCliente);
            ps1.setInt(1, ct.getId_cliente());
            ps1.setString(2, ct.getNombre_cliente());
            ps1.setString(3, ct.getApellido_cliente());
            ps1.setString(4, ct.getDireccion());
            ps1.setString(5, ct.getLocalidad());
            ps1.setString(6, ct.getTelefono());
            ps1.setString(7, ct.getMail());
            int n = ps1.executeUpdate();

            if (n > 0) {
                JOptionPane.showMessageDialog(null, "Registro guardado con exito");
                con.close();
            }

        } catch (SQLException ex) {
            Logger.getLogger(manejadorCliente.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public Cliente traerCliente(int id_cliente){
        Cliente cli = new Cliente();
        conexionDB c = new conexionDB();
        Connection con = c.conectar();
        
        String sql = "SELECT id_cliente, nombre_cliente, apellido_cliente, telefono, direccion, localidad from Cliente "
                + "WHERE id_cliente = "+id_cliente;
        
        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                cli.setId_cliente(id_cliente);
                cli.setNombre_cliente(rs1.getString(2));
                cli.setApellido_cliente(rs1.getString(3));
                cli.setTelefono(rs1.getString(4));
                cli.setDireccion(rs1.getString(5));
                cli.setLocalidad(rs1.getString(6));              

            }

            con.close();

        } catch (Exception e) {
            System.err.println("ERROR TRAYENDO CLIENTE: " + e);
        }
        
        
        return cli;
    }
    
    public void traerClientesTabla(JTable tablaClientes, DefaultTableModel modelo){
        DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
        centerRenderer.setHorizontalAlignment( JLabel.CENTER );
        tablaClientes.setModel(modelo);
        
        modelo.addColumn("DNI");
        modelo.addColumn("Cliente");
        modelo.addColumn("Telefono");             

        tablaClientes.getColumnModel().getColumn(0).setCellRenderer(centerRenderer);
        tablaClientes.getColumnModel().getColumn(1).setCellRenderer(centerRenderer);
        tablaClientes.getColumnModel().getColumn(2).setCellRenderer(centerRenderer);

        Object[] articulo = new Object[3];

        String sql = "SELECT id_cliente, nombre_cliente, apellido_cliente, telefono from Cliente";
        conexionDB c = new conexionDB();
        Connection con = c.conectar();

        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                articulo[0] = rs1.getString(1);
                articulo[1] = rs1.getString(2) + " " + rs1.getString(3);
                articulo[2] = rs1.getString(4);

                modelo.addRow(articulo);
            }

            con.close();

        } catch (Exception e) {
            System.err.println("ERROR TRAYENDO CLIENTES: " + e);
        }
        
    }
    
    public Cliente obtenerCliente(String bus, Cliente cli) {

        String sql = "SELECT c.id_cliente, c.apellido_cliente, c.nombre_cliente, c.telefono, c.direccion, c.localidad "
                + "from cliente as c "
                + "where CONCAT(c.apellido_cliente, ' ' , c.nombre_cliente) LIKE '%" + bus + "%'";

        String codigo = "";

        System.out.println(sql);

        conexionDB c = new conexionDB();
        Connection con = c.conectar();

        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                cli.setId_cliente(rs1.getInt(1));
                cli.setNombre_cliente(rs1.getString(3));
                cli.setApellido_cliente(rs1.getString(2));
                cli.setDireccion(rs1.getString(5));
                cli.setLocalidad(rs1.getString(6));
                cli.setTelefono(rs1.getString(4));
            }

        } catch (Exception e) {
            System.out.println("Error: " + e);
        }
        
        return cli;
    }
    
    public void traerClientesComboBox(JComboBox jcClientes){
        String sqlClientes = "SELECT CONCAT(apellido_cliente, ' ', nombre_cliente) from cliente";
        conexionDB c = new conexionDB();
        Connection con = c.conectar();
        
        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sqlClientes);

            while (rs1.next()) {
                jcClientes.addItem(rs1.getString(1));
            }

            jcClientes.setSelectedIndex(-1);
            

            con.close();
        } catch (SQLException ex) {            
        }
    }
    
    public boolean existeCliente(int id) {
        boolean b = false;
        conexionDB c = new conexionDB();
        Connection con = c.conectar();
        String sql = "select * from cliente where id_cliente = "+id;
        System.out.println(sql);
        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                b = true;
            }
        } catch (SQLException ex) {
            Logger.getLogger(manejadorArticulos.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println("BANDERA: " + b);
        return b;
    }
    
    public void pagarCuota (int id_detallectacte){
        
        
    }
    
}
