-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1:3306
-- Tiempo de generación: 31-05-2019 a las 23:10:04
-- Versión del servidor: 5.7.21
-- Versión de PHP: 5.6.35

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `db_mariabonita`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `articulo`
--

DROP TABLE IF EXISTS `articulo`;
CREATE TABLE IF NOT EXISTS `articulo` (
  `codigo` varchar(8) NOT NULL,
  `id_tipo` int(11) NOT NULL,
  `id_talle` int(11) NOT NULL,
  `precio_costo` float DEFAULT NULL,
  `precio_venta` float DEFAULT NULL,
  `stock` int(11) DEFAULT NULL,
  `desc_articulo` varchar(50) NOT NULL,
  PRIMARY KEY (`codigo`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `articulo`
--

INSERT INTO `articulo` (`codigo`, `id_tipo`, `id_talle`, `precio_costo`, `precio_venta`, `stock`, `desc_articulo`) VALUES
('RS-345', 17, 2, 1400, 2800, 1, 'Vestido de Fiesta Negro'),
('RX-22', 14, 9, 150, 250, 1, 'Remera cuello en V Blanca'),
('RS22', 15, 9, 123, 245, 1, 'REMERA'),
('VE234', 17, 2, 1200, 2500, 1, 'Vestido Largo de Gaza Verde'),
('VX23', 17, 9, 2000, 3400, 1, 'Vestido Negro Escotado');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `boleta`
--

DROP TABLE IF EXISTS `boleta`;
CREATE TABLE IF NOT EXISTS `boleta` (
  `id_boleta` int(11) NOT NULL,
  `fecha_boleta` date NOT NULL,
  `id_cliente` int(11) NOT NULL,
  `id_detalleboleta` int(11) NOT NULL,
  PRIMARY KEY (`id_boleta`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `caja_diaria`
--

DROP TABLE IF EXISTS `caja_diaria`;
CREATE TABLE IF NOT EXISTS `caja_diaria` (
  `id_caja` int(11) NOT NULL AUTO_INCREMENT,
  `fecha_caja` date NOT NULL,
  PRIMARY KEY (`id_caja`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliente`
--

DROP TABLE IF EXISTS `cliente`;
CREATE TABLE IF NOT EXISTS `cliente` (
  `id_cliente` int(11) NOT NULL,
  `nombre_cliente` varchar(20) NOT NULL,
  `apellido_cliente` varchar(20) NOT NULL,
  `direccion` varchar(30) NOT NULL,
  `localidad` varchar(20) NOT NULL,
  `telefono` varchar(20) NOT NULL,
  `mail` varchar(30) NOT NULL,
  PRIMARY KEY (`id_cliente`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `cliente`
--

INSERT INTO `cliente` (`id_cliente`, `nombre_cliente`, `apellido_cliente`, `direccion`, `localidad`, `telefono`, `mail`) VALUES
(37305672, 'Rodri', 'Mayer', 'Maipu', 'LGSM', '3886525523', 'rodrimmayer@gmail.com'),
(12124155, 'Maria Agostina', 'Rubio', 'Santa Rosa', 'LGSM', '3886555969', 'agosrubio@gmail.com');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `color`
--

DROP TABLE IF EXISTS `color`;
CREATE TABLE IF NOT EXISTS `color` (
  `id_color` int(11) NOT NULL,
  `desc_color` varchar(20) NOT NULL,
  PRIMARY KEY (`id_color`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cond_vta`
--

DROP TABLE IF EXISTS `cond_vta`;
CREATE TABLE IF NOT EXISTS `cond_vta` (
  `id_condvta` int(11) NOT NULL,
  `desc_condvta` varchar(20) NOT NULL,
  PRIMARY KEY (`id_condvta`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cta_cte`
--

DROP TABLE IF EXISTS `cta_cte`;
CREATE TABLE IF NOT EXISTS `cta_cte` (
  `id_ctacte` int(11) NOT NULL,
  `id_cliente` int(11) NOT NULL,
  `deuda` float NOT NULL,
  PRIMARY KEY (`id_ctacte`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle_boleta`
--

DROP TABLE IF EXISTS `detalle_boleta`;
CREATE TABLE IF NOT EXISTS `detalle_boleta` (
  `id_detalleboleta` int(11) NOT NULL,
  `id_boleta` int(11) NOT NULL,
  `codigo` varchar(20) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `precio_total` float NOT NULL,
  PRIMARY KEY (`id_detalleboleta`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle_caja`
--

DROP TABLE IF EXISTS `detalle_caja`;
CREATE TABLE IF NOT EXISTS `detalle_caja` (
  `id_detallecaja` int(11) NOT NULL AUTO_INCREMENT,
  `id_caja` int(11) NOT NULL,
  `id_condvta` int(11) NOT NULL,
  `total` float NOT NULL,
  PRIMARY KEY (`id_detallecaja`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle_cuenta`
--

DROP TABLE IF EXISTS `detalle_cuenta`;
CREATE TABLE IF NOT EXISTS `detalle_cuenta` (
  `id_ctacte` int(11) NOT NULL AUTO_INCREMENT,
  `id_cliente` int(11) NOT NULL,
  `total` float NOT NULL,
  PRIMARY KEY (`id_ctacte`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `facturacion_boleta`
--

DROP TABLE IF EXISTS `facturacion_boleta`;
CREATE TABLE IF NOT EXISTS `facturacion_boleta` (
  `id_facturacionboleta` int(11) NOT NULL AUTO_INCREMENT,
  `id_boleta` int(11) NOT NULL,
  `id_condvta` varchar(10) NOT NULL,
  `monto` float NOT NULL,
  PRIMARY KEY (`id_facturacionboleta`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `localidad`
--

DROP TABLE IF EXISTS `localidad`;
CREATE TABLE IF NOT EXISTS `localidad` (
  `id_localidad` int(11) NOT NULL AUTO_INCREMENT,
  `localidad` varchar(20) NOT NULL,
  PRIMARY KEY (`id_localidad`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `talle`
--

DROP TABLE IF EXISTS `talle`;
CREATE TABLE IF NOT EXISTS `talle` (
  `id_talle` int(11) NOT NULL AUTO_INCREMENT,
  `desc_talle` varchar(20) NOT NULL,
  PRIMARY KEY (`id_talle`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `talle`
--

INSERT INTO `talle` (`id_talle`, `desc_talle`) VALUES
(1, 'S'),
(2, 'M'),
(5, 'L'),
(6, 'XL'),
(9, 'Unico'),
(10, '34');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo`
--

DROP TABLE IF EXISTS `tipo`;
CREATE TABLE IF NOT EXISTS `tipo` (
  `desc_tipo` varchar(20) NOT NULL,
  `id_tipo` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id_tipo`)
) ENGINE=MyISAM AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tipo`
--

INSERT INTO `tipo` (`desc_tipo`, `id_tipo`) VALUES
('Vestido Largo', 17),
('Pollera', 16),
('Buzo', 15),
('Remera', 14);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
