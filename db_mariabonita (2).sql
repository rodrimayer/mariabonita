-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 04-06-2019 a las 21:43:07
-- Versión del servidor: 5.5.24-log
-- Versión de PHP: 5.4.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `db_mariabonita`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `articulo`
--

CREATE TABLE IF NOT EXISTS `articulo` (
  `codigo` varchar(8) NOT NULL,
  `id_tipo` int(11) NOT NULL,
  `id_talle` int(11) NOT NULL,
  `precio_costo` float DEFAULT NULL,
  `precio_venta` float DEFAULT NULL,
  `stock` int(11) DEFAULT NULL,
  `desc_articulo` varchar(50) NOT NULL,
  PRIMARY KEY (`codigo`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `articulo`
--

INSERT INTO `articulo` (`codigo`, `id_tipo`, `id_talle`, `precio_costo`, `precio_venta`, `stock`, `desc_articulo`) VALUES
('RS-345', 17, 2, 1400, 2800, 1, 'Vestido de Fiesta Negro'),
('RX-22', 14, 9, 150, 250, 1, 'Remera cuello en V Blanca'),
('RI5', 14, 6, 300, 750, 1, 'Remera Color Crema'),
('VU30', 17, 2, 850, 1900, 1, 'Vestido Corto Verde Marca Las Locas'),
('XR242', 14, 1, 120, 400, 1, 'Remera básica negra MariaBonita');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `boleta`
--

CREATE TABLE IF NOT EXISTS `boleta` (
  `id_boleta` int(11) NOT NULL AUTO_INCREMENT,
  `fecha_boleta` date NOT NULL,
  `id_cliente` int(11) NOT NULL,
  PRIMARY KEY (`id_boleta`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Volcado de datos para la tabla `boleta`
--

INSERT INTO `boleta` (`id_boleta`, `fecha_boleta`, `id_cliente`) VALUES
(4, '2019-06-04', 37305672),
(5, '2019-06-04', 1234555);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `caja_diaria`
--

CREATE TABLE IF NOT EXISTS `caja_diaria` (
  `id_caja` int(11) NOT NULL AUTO_INCREMENT,
  `fecha_caja` date NOT NULL,
  PRIMARY KEY (`id_caja`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliente`
--

CREATE TABLE IF NOT EXISTS `cliente` (
  `id_cliente` int(11) NOT NULL,
  `nombre_cliente` varchar(20) NOT NULL,
  `apellido_cliente` varchar(20) NOT NULL,
  `direccion` varchar(30) NOT NULL,
  `localidad` varchar(20) NOT NULL,
  `telefono` varchar(20) NOT NULL,
  `mail` varchar(30) NOT NULL,
  PRIMARY KEY (`id_cliente`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `cliente`
--

INSERT INTO `cliente` (`id_cliente`, `nombre_cliente`, `apellido_cliente`, `direccion`, `localidad`, `telefono`, `mail`) VALUES
(37305672, 'Rodri', 'Mayer', 'Maipu', 'LGSM', '3886525523', 'rodrimmayer@gmail.com'),
(1234555, 'Martin', 'Perez', 'Juan XXIII', 'LGSM', '123213123', 'adssadad'),
(12321455, 'Maria Agostina', 'Rubio', 'Barrio Santa Rosa', 'LGSM', '12312552', 'sadsadsad');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `color`
--

CREATE TABLE IF NOT EXISTS `color` (
  `id_color` int(11) NOT NULL,
  `desc_color` varchar(20) NOT NULL,
  PRIMARY KEY (`id_color`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cond_vta`
--

CREATE TABLE IF NOT EXISTS `cond_vta` (
  `id_condvta` int(11) NOT NULL AUTO_INCREMENT,
  `desc_condvta` varchar(20) NOT NULL,
  `permite_cuotas` tinyint(1) NOT NULL,
  PRIMARY KEY (`id_condvta`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Volcado de datos para la tabla `cond_vta`
--

INSERT INTO `cond_vta` (`id_condvta`, `desc_condvta`, `permite_cuotas`) VALUES
(1, 'VISA DEBITO', 0),
(2, 'VISA CREDITO', 1),
(3, 'MASTERCARD DEBITO', 0),
(4, 'MASTERCARD CREDITO', 1),
(5, 'NARANJA DEBITO', 0),
(6, 'NARANJA CREDITO', 1),
(7, 'MERCADOPAGO', 0),
(8, 'CONTADO', 0),
(9, 'CUENTA CORRIENTE', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cta_cte`
--

CREATE TABLE IF NOT EXISTS `cta_cte` (
  `id_ctacte` int(11) NOT NULL,
  `id_cliente` int(11) NOT NULL,
  `deuda` float NOT NULL,
  PRIMARY KEY (`id_ctacte`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle_boleta`
--

CREATE TABLE IF NOT EXISTS `detalle_boleta` (
  `id_detalleboleta` int(11) NOT NULL AUTO_INCREMENT,
  `id_boleta` int(11) NOT NULL,
  `codigo` varchar(20) NOT NULL,
  `descuento` int(11) NOT NULL,
  `precio_total` float NOT NULL,
  PRIMARY KEY (`id_detalleboleta`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Volcado de datos para la tabla `detalle_boleta`
--

INSERT INTO `detalle_boleta` (`id_detalleboleta`, `id_boleta`, `codigo`, `descuento`, `precio_total`) VALUES
(3, 4, 'RI5', 0, 750),
(4, 4, 'RX-22', 40, 150),
(5, 4, 'RS-345', 20, 2240),
(6, 4, 'VU30', 0, 1900),
(7, 5, 'RI5', 0, 750),
(8, 5, 'RX-22', 0, 250),
(9, 5, 'VU30', 30, 1330);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle_caja`
--

CREATE TABLE IF NOT EXISTS `detalle_caja` (
  `id_detallecaja` int(11) NOT NULL AUTO_INCREMENT,
  `id_caja` int(11) NOT NULL,
  `id_condvta` int(11) NOT NULL,
  `cant_cuotas` int(11) NOT NULL,
  `total` float NOT NULL,
  PRIMARY KEY (`id_detallecaja`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle_cuenta`
--

CREATE TABLE IF NOT EXISTS `detalle_cuenta` (
  `id_ctacte` int(11) NOT NULL AUTO_INCREMENT,
  `id_cliente` int(11) NOT NULL,
  `total` float NOT NULL,
  PRIMARY KEY (`id_ctacte`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `facturacion_boleta`
--

CREATE TABLE IF NOT EXISTS `facturacion_boleta` (
  `id_facturacionboleta` int(11) NOT NULL AUTO_INCREMENT,
  `id_boleta` int(11) NOT NULL,
  `id_condvta` varchar(10) NOT NULL,
  `cant_cuotas` int(11) NOT NULL,
  `monto` float NOT NULL,
  PRIMARY KEY (`id_facturacionboleta`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Volcado de datos para la tabla `facturacion_boleta`
--

INSERT INTO `facturacion_boleta` (`id_facturacionboleta`, `id_boleta`, `id_condvta`, `cant_cuotas`, `monto`) VALUES
(7, 4, '1', 0, 2000),
(8, 4, '6', 6, 3040),
(9, 5, '8', 0, 800),
(10, 5, '9', 0, 500),
(11, 5, '6', 1, 1030);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `localidad`
--

CREATE TABLE IF NOT EXISTS `localidad` (
  `id_localidad` int(11) NOT NULL AUTO_INCREMENT,
  `localidad` varchar(20) NOT NULL,
  PRIMARY KEY (`id_localidad`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `talle`
--

CREATE TABLE IF NOT EXISTS `talle` (
  `id_talle` int(11) NOT NULL AUTO_INCREMENT,
  `desc_talle` varchar(20) NOT NULL,
  PRIMARY KEY (`id_talle`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Volcado de datos para la tabla `talle`
--

INSERT INTO `talle` (`id_talle`, `desc_talle`) VALUES
(1, 'S'),
(2, 'M'),
(5, 'L'),
(6, 'XL'),
(9, 'Unico'),
(10, '34');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo`
--

CREATE TABLE IF NOT EXISTS `tipo` (
  `desc_tipo` varchar(20) NOT NULL,
  `id_tipo` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id_tipo`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18 ;

--
-- Volcado de datos para la tabla `tipo`
--

INSERT INTO `tipo` (`desc_tipo`, `id_tipo`) VALUES
('Vestido Largo', 17),
('Pollera', 16),
('Buzo', 15),
('Remera', 14);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
